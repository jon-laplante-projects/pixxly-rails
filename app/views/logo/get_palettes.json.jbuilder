json.palettes @palettes.each do |palette|
  json.id palette.id
  json.label palette.label

  json.colors palette.palette_colors.each do |color|
    json.id color.id
    json.color color.color
  end
end