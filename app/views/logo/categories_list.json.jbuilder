json.categories @categories.each do |category|
  json.category_id category.id
  json.category_label category.label

  json.logos category.logos.each do |logo|
    json.id logo.id
    json.preview_img "previews/prev_#{logo.id}.png"
    # json.svg logo.svg
  end
end