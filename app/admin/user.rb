ActiveAdmin.register User do
  filter :first_name
  filter :last_name
  filter :email
  filter :frozen
  filter :cancelled
  filter :bonus
  filter :reset_password_sent_at
  filter :current_sign_in_at
  filter :last_sign_in_at
  filter :created_at
  filter :updated_at

controller do
  def update
    if params[:user][:password].blank?
      params[:user].delete("password")
      params[:user].delete("password_confirmation")
    end
    super
  end
end

index do
  column :first_name
  column :last_name
  column :email
  column :frozen
  column :cancelled
  column :bonus
  column :reset_password_sent_at
  column :current_sign_in_at
  column :last_sign_in_at
  column :current_sign_in_ip
  column :last_sign_in_ip
  column :created_at
  column :updated_at
  actions
end

form do |f|
  f.inputs do
    f.input :first_name
    f.input :last_name
    f.input :email
    f.input :password
    f.input :password_confirmation
    f.input :frozen
    f.input :cancelled
    f.input :bonus
  end

  f.actions
end

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
permit_params :first_name, :last_name, :email, :frozen, :cancelled, :bonus, :password, :password_confirmation
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end


end
