ActiveAdmin.register Layout do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
permit_params :label, :svg, :image
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end

  index as: :grid do |layout|
    link_to image_tag("#{layout.image}.png", style: "width: 150px;"), admin_layout_path(layout)
  end
end
