ActiveAdmin.register Lead do
  permit_params :first_name, :surname, :email, :phone, :opt_out

  active_admin_import  headers_rewrites: { :'last_name' => :surname, 'LastName' => :surname, 'lastName' => :surname, 'lastname' => :surname, 'firstname' => :first_name, 'firstName' => :first_name, 'name' => :first_name, 'FirstName' => :first_name }
end