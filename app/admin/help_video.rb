ActiveAdmin.register HelpVideo do
  permit_params :label, :descr, :embed_code
end