ActiveAdmin.register Email do
  permit_params :name, :subj, :content, :list_type

  index do
    column :created_at
    column :name
    column :subj
    column :content
    column :list_type
    column :updated_at
    actions
  end

  form do |f|
    f.inputs do
      f.input :name
      f.input :subj, label: "Subject"
      f.input :content, as: :html_editor
      f.input :list_type, as: :radio, collection: ["leads", "users", "both"], input_html: { required: true }
    end

    f.actions
  end
end