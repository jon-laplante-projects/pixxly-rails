ActiveAdmin.register_page "Dashboard" do

  menu priority: 1, label: proc{ I18n.t("active_admin.dashboard") }

  content title: proc{ I18n.t("active_admin.dashboard") } do
    latest_users = User.all.limit(10).order("id DESC")
    user_count = User.all.count

    h2 link_to "Pixxly Users", "/admin/users"
    h4 "User Count: #{user_count}"
    h3 "Most recent"
    div do
      table_for latest_users do
        column "First Name",      :first_name
        column "Last Name",       :last_name
        column "Email",           :email
        column "Account Created", :created_at
      end

      text_node link_to "View all users", "/admin/users"
    end
    # div class: "blank_slate_container", id: "dashboard_default_message" do
    #   span class: "blank_slate" do
    #     span I18n.t("active_admin.dashboard_welcome.welcome")
    #     small I18n.t("active_admin.dashboard_welcome.call_to_action")
    #   end
    # end

    # Here is an example of a simple dashboard with columns and panels.
    #
    # columns do
    #   column do
    #     panel "Recent Posts" do
    #       ul do
    #         Post.recent(5).map do |post|
    #           li link_to(post.title, admin_post_path(post))
    #         end
    #       end
    #     end
    #   end

    #   column do
    #     panel "Info" do
    #       para "Welcome to ActiveAdmin."
    #     end
    #   end
    # end
  end # content
end
