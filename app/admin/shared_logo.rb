ActiveAdmin.register SharedLogo do
  index as: :grid do |logo|
    link_to logo.svg.html_safe, admin_shared_logo_path(logo)
  end
end