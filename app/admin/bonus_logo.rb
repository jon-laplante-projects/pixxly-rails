ActiveAdmin.register BonusLogo do
  active_admin_import
  
  index as: :grid do |logo|
    link_to logo.svg.html_safe, admin_bonus_logo_path(logo)
  end
  
  permit_params :svg
end