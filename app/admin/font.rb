ActiveAdmin.register Font do
  permit_params :name, :url, :import, :font_face
  
  active_admin_import
end