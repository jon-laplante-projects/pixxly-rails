class BonusLogo < ActiveRecord::Base

  def newSize(firstG) # Transforms moved from .js to model.
    
  end
  
  def attr_transform # Transforms moved from .js to model for bonus logos.
    svg = Nokogiri::XML(self.svg)
    
    firstG = svg.css("g")[1]
    g = {}
    g["width"] = firstG["width"]
    g["height"] = firstG["height"]
    g["x"] = firstG["x"]
    g["y"] = firstG["y"]
  end
end