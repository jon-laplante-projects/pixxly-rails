class Email < ActiveRecord::Base
  after_save :send_message
  after_initialize :set_defaults, unless: :persisted?

  def set_defaults
    self.list_type ||= "leads"
  end

  def send_message
    contacts = []

    case self.list_type
      when "leads"
        leads = Lead.where(opt_out: false)

        leads.each do |lead|
          contact = {}
          contact["first_name"] = lead.first_name
          contact["surname"] = lead.surname
          contact["email"] = lead.email
          contacts << contact
        end

      when "users"
        users = User.all

        users.each do |user|
          contact = {}
          contact["first_name"] = user.first_name
          contact["surname"] = user.last_name
          contact["email"] = user.email
          contacts << contact
        end

      when "both"
        leads = Lead.select(:first_name, :surname, :email).where(opt_out: false)
        users = User.select(:first_name, :last_name, :email).all

        leads.each do |lead|
          contact = {}
          contact["first_name"] = lead.first_name
          contact["surname"] = lead.surname
          contact["email"] = lead.email
          contacts << contact
        end

        users.each do |user|
          contact = {}
          contact["first_name"] = user.first_name
          contact["surname"] = user.last_name
          contact["email"] = user.email
          contacts << contact
        end
    end

      ses = AWS::SES::Base.new(
          :access_key_id     => "AKIAIFLK45XNMCED525A",
          :secret_access_key => "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"
      )

      contacts.each do |recipient|
        begin
          result = ses.send_email(
              :to        => ["#{recipient["email"]}"],
              :source    => "'noreply@pixxly.net' <noreply@pixxly.net>",
              :subject   => "#{self.subj}",
              :html_body => "#{self.content}"
          )

          result_hash = Hash.from_xml(result)

          sent = SentMsg.new
          sent.lead_id = recipient["id"]
          sent.mail_id = self.id
          sent.status = "attempt"

          unless result_hash["SendEmailResponse"]["SendEmailResult"]["MessageId"].blank?
            sent.status = "sent"
            sent.save
          end
        rescue => e
          byebug
          sent.status = "failed"
          sent.save
          return false
        end
      end

  end
end
