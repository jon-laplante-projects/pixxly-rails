class ExperimentalController < ApplicationController
  def index

  end

  def raster_to_svg
    # Convert using minimagick.
    img = params[:image_url]
    img_file = img.split("/").last
    img_name = File.basename(img_file,File.extname(img_file))
    system "rm ./public/#{img_name.to_s}.svg"
    system "autotrace -output-file ./public/#{img_name}.svg -output-format svg --color-count 256 --despeckle-level 20 --despeckle-tightness 8.0 --line-threshold 0.01 --corner-surround 100 --filter-iterations 1 --line-reversion-threshold 100 #{img}"

    render json: {url: "#{img_name.to_s}.svg", success: true}
  end

  def svg_to_jpg
    img = params[:image_url]
    img_file = img.split("/").last
    img_name = File.basename(img_file,File.extname(img_file))

    system "rm ./public/#{img_name.to_s}.jpg"

    system "convert #{img} ./public/#{img_name}.jpg"

    render json: {url: "#{img_name.to_s}.jpg", success: true}
  end

  def svg_to_psd
    img = params[:image_url]
    img_file = img.split("/").last
    img_name = File.basename(img_file,File.extname(img_file))

    system "rm ./public/#{img_name.to_s}.psd"

    system "convert #{img} ./public/#{img_name}.psd"

    render json: {url: "#{img_name.to_s}.psd", success: true}
  end

  def svg_to_raster
    img = params[:image_url]
    img_file = img.split("/").last
    img_name = File.basename(img_file,File.extname(img_file))

    system "rm ./public/#{img_name.to_s}.#{params[:file_type]}"

    system "convert #{img} ./public/#{img_name}.#{params[:file_type]}"

    render json: {url: "#{img_name.to_s}.#{params[:file_type]}", success: true}
  end

  def logo_ocr
    img = params[:image_url]

    image = RTesseract.new("./app/assets/images/image_with_text.png", processor: "mini_magick")

    text = image.to_s

    render json: { success: true, scanned: text }
  end
end