require 'open-uri'
require 'tempfile'

class LogoController < ApplicationController
  include LogoHelper

  before_action :authenticate_user!, except: [:as_file, :user_logo_as_file]
  XML_PREFIX = "<?xml version=\"1.0\"?><!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\" \"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">"

  def index
    @user_logos = current_user.user_logos.order("updated_at DESC").page(params[:page]).per(9)
    @user_logos = current_user.user_logos.order("updated_at DESC").page(@user_logos.total_pages).per(9) if @user_logos.empty?
  end

  def symbols_set
    cats = CategoriesLogos.select(:logo_id).where(category_id: params[:category_id])
    # symbols_set = Logo.where(["id IN (?)", cats]).pluck(:id)

    render partial: "symbols_set", locals: {symbols_set: cats}
    # render partial: "symbols_set", locals: {symbols_set: symbols_set}
  end

  def bonus
    if current_user.bonus?
      @bonus_count = BonusLogo.find_by_sql(["SELECT * FROM bonus_logos WHERE user_logo_id IN (SELECT id FROM user_logos WHERE user_id = ?);", current_user.id]).count
      @bonus = BonusLogo.page(params[:page]).order("ID DESC").per(9)
    else
      redirect_to "/logo"
    end
  end

  def library
    @shared_count = SharedLogo.find_by_sql(["SELECT * FROM shared_logos WHERE user_logo_id IN (SELECT id FROM user_logos WHERE user_id = ?);", current_user.id]).count
    # @shared_count = current_user.shared_logos.count
    @shared = SharedLogo.page(params[:page]).order("ID DESC").per(9)
  end

  def import_from_library
    unless params[:shared_id].blank?
      logo = SharedLogo.find(params[:shared_id])
      new_logo = current_user.user_logos.new

      new_logo.svg = logo.svg
      new_logo.layout_template_id = nil
      if new_logo.save
        render json: { success: true }, status: 200
      else
        render json: { success: false }, status: 200
      end
    else
      render json: { success: false }, status: 200
    end
  end
  
  def import_from_bonus
    unless params[:bonus_id].blank?
      logo = BonusLogo.find(params[:bonus_id])
      new_logo = current_user.user_logos.new

      new_logo.svg = logo.svg
      new_logo.layout_template_id = nil
      if new_logo.save
        render json: { success: true }, status: 200
      else
        render json: { success: false }, status: 200
      end
    else
      render json: { success: false }, status: 200
    end
  end

  def library_editor
    #@user_logo = UserLogo.new

    unless params[:library_logo_id].blank?
      @fonts = Font.select(:id, :name, :font_face).all
      @bonus_fonts = BonusFont.select(:id, :name, :font_face).all
      @svg = SharedLogo.find(params[:library_logo_id])
    else
      @svg = nil
    end
  end

  def clone_user_logo
    unless params[:logo_id].blank?
      logo = UserLogo.find(params[:logo_id])
      new_logo = UserLogo.new

      new_logo.svg = logo.svg
      new_logo.logo_id = logo.logo_id
      # new_logo.preview = logo.preview
      new_logo.user_id = logo.user_id
      new_logo.layout_template_id = logo.layout_template_id

      if new_logo.save
        shared = false
        shared_lbl = "Share"
        svg_xml = Nokogiri::XML.parse(logo.svg.to_s)
        svg = logo.svg.to_s

        unless svg_xml.at_xpath("//xmlns:svg/@viewBox")
          svg.gsub!('xmlns="http://www.w3.org/2000/svg"', 'xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid meet" class="svgElement"')
        end

        inserted_panel = <<-PANEL
          <div class="panel panel-default" id="logo_panel_#{ new_logo.id }" style="width: 360px; margin: 6px 6px 6px 6px;">
            <div class="panel-heading clearfix">
              <h3 class="panel-title pull-left"></h3>
              <div class="btn-group pull-right" role="group">
                <a class="btn btn-default" href="/edit/#{ new_logo.id }">
                  <i class="fa fa-check"></i>
                  Edit
                </a>
                <a class="btn btn-default share-logo-btn" href="#" data-id="#{ new_logo.id }" data-status=#{ shared }>
                  <i class="fa fa-share-alt"></i>#{ shared_lbl }
                </a>
                <button type="button" class="btn btn-default copy-logo-btn" data-id="#{ new_logo.id }">
                  <i class="fa fa-clone"></i>
                  Clone
                </button>
                <a class="btn btn-default delete-logo-btn" href="#" data-id="#{ new_logo.id }">
                  <i class="fa fa-trash"></i>
                  Delete
                </a>
              </div>
            </div>

            <div class="panel-body" style="height: 429px; display: flex; justify-content: center; background-color: white;" data-id="#{ new_logo.id }" data-dimensions="">
              #{ svg.html_safe }
              </div>
            <div class="panel-footer">
              <div class="btn-group btn-container" role="group" aria-label="">
                <a href="#" class="btn btn-default conversion_btn" data-id="#{ new_logo.id }" data-file-type="png" data-toggle="modal" data-target="#logoDownloadSizeModal">PNG</a>
                <a href="#" class="btn btn-default conversion_btn" data-id="#{ new_logo.id }" data-file-type="jpg" data-toggle="modal" data-target="#logoDownloadSizeModal">JPG</a>
                <a href="#" class="btn btn-default conversion_btn" data-id="#{ new_logo.id }" data-file-type="gif" data-toggle="modal" data-target="#logoDownloadSizeModal">GIF</a>
                <a target="_BLANK" download href="/user/logo/#{ new_logo.id }/as/file.svg" class="btn btn-default conversion_btn" data-id="#{ new_logo.id }">SVG</a>
              </div>
              <div class="btn-group btn-container" role="group" aria-label="" style="margin-top: 10px;">
                <a href="#" class="btn btn-default conversion_btn" data-id="#{ new_logo.id }" data-file-type="psd" data-toggle="modal" data-target="#logoDownloadSizeModal">PSD</a>
                <a href="#" class="btn btn-default conversion_btn" data-id="#{ new_logo.id }" data-file-type="tiff" data-toggle="modal" data-target="#logoDownloadSizeModal">TIFF</a>
                <a href="#" class="btn btn-default conversion_btn" data-id="#{ new_logo.id }" data-file-type="wmf" data-toggle="modal" data-target="#logoDownloadSizeModal">WMF</a>
              </div>
            </div>
          </div>
        PANEL

        render json: { success: true, id: new_logo.id, panel: inserted_panel }, status: 200
      else
        render json: { success: false, message: "Unable to clone this logo." }, status: 200
      end

    else
      render json: { success: false, message: "Please select a logo to clone." }, status: 200
    end
  end

  def new
    # If there is no expires_on date for the user, compare today to tomorrow.
    expires = current_user.expires_on || Date.today + 1.day
    if expires < Date.today
      redirect_to "index"
    else
      @categories = Category.all
      @palettes = Palette.all.includes(:palette_colors)
      @layouts = Layout.all.order(:id)
      @fonts = Font.select(:id, :name, :font_face).all
      @bonus_fonts = BonusFont.select(:id, :name, :font_face).all

      # If we've got a symbol/category, get them.
      unless params[:sy].blank?
        @symbol_id = params[:sy]

        @sym_cat_id = CategoriesLogos.select(:category_id).find_by(logo_id: @symbol_id).category_id
        @active_category = Category.find(@sym_cat_id)
      else
        @active_category = @categories.first
      end

      # If we've got a layout selected, get it.
      unless params[:la].blank?
        @layout_id = params[:la]
      end

      # If we've got a color palette selected, get it.
      unless params[:pl].blank?
        @palette_id = params[:pl]
      end

      # If we've got a tab selected, get it.
      unless params[:pg].blank?
        @tab_id = params[:pg]
      end

      # If we've got an OCA id, get it.
      unless params[:oca].blank?
        @oca_id = params[:oca]
      end

      # If we've got an OCA search criteria, get it.
      unless params[:ocas].blank?
        @oca_query = params[:ocas]
      end

      # If we've got a page in the OCA pagination selected, get it.
      unless params[:ocap].blank?
        @oca_page = params[:ocap]
      end
    end
  end

  def editor
    render layout: false
  end

  def embed_raster_to_svg
    @svg = { image: params[:image], id: nil }
    render 'edit_logo'
  end

  def store_raster
    unless params[:url].blank? && params[:path].blank?
      if params[:url].blank? == false && params[:url] != "null"
        uploaded_io = params[:url]
        img = MiniMagick::Image.open("#{uploaded_io}")

        filename = uploaded_io.to_s.split("/").last
        filetype = filename.split(".").last

        img.write "tmp/#{filename}"

        base64_data = Base64.encode64(File.read("tmp/#{filename}"))

        # Generate a new SVG image with this embedded.
        svg = <<-DOC
          <svg width="400" height="400" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
            <metadata>image/svg+xml</metadata>
            <g id="raster_layer">
            <image id="imported_raster" width="400" height="400" xlink:href="data:image/#{filetype};base64,#{base64_data}" />
            </g>
          </svg>
        DOC

        # Save the logo to the users logos.
        user_logo = current_user.user_logos.new

        user_logo.svg = svg
        user_logo.save

        img.destroy!

        render json: { success: true }, status: 200
      elsif params[:path].blank? === false
        uploaded_io = params[:path]
        directory_name = 'public/uploads'
        Dir.mkdir directory_name unless Dir.exists?(directory_name)
        file_path = Rails.root.join(directory_name, uploaded_io.original_filename)

        File.open(file_path, 'wb') do |file|
          file.write(uploaded_io.read)
        end

        img = MiniMagick::Image.open("#{file_path}")

        filename = file_path.to_s.split("/").last
        filetype = filename.split(".").last

        img.write "tmp/#{filename}"

        base64_data = Base64.encode64(File.read("tmp/#{filename}"))

        # Generate a new SVG image with this embedded.
        svg = <<-DOC
          <svg width="400" height="400" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
            <metadata>image/svg+xml</metadata>
            <g id="raster_layer">
            <image id="imported_raster" width="400" height="400" xlink:href="data:image/#{filetype};base64,#{base64_data}" />
            </g>
          </svg>
        DOC

        # Save the logo to the users logos.
        user_logo = current_user.user_logos.new

        user_logo.svg = svg
        user_logo.save

        img.destroy!

        render json: { success: true }, status: 200
      end
    else
      render json: {success: false}, status: 200
    end
  end

  def edit_logo
    @fonts = Font.select(:id, :name, :font_face).all
    @bonus_fonts = BonusFont.select(:id, :name, :font_face).all

    unless params[:user_logo_id].blank?
      @svg = current_user.user_logos.find(params[:user_logo_id])
    else
      @svg = nil
    end
  end

  def clone
    # Convert using minimagick.
    if params[:img]
      uploaded_io = params[:img]
      file_path = Rails.root.join('public', uploaded_io.original_filename)
      File.open(file_path, 'wb') do |file|
        file.write(uploaded_io.read)
      end
    else
      file_path = params[:image_url]
    end

    img = MiniMagick::Image.open(file_path)


    # img = params[:image_url]
    # img_file = img.split("/").last
    img_file = img.path.split("/").last
    img_name = File.basename(img_file,File.extname(img_file))
    img.resize "400x400"
    # img.resize "500x500"
    img.colors 24
    # img.colors 32
    # result_img = "./public/svg/#{img_name}.svg"
    result_img = "./public/svg/#{current_user.id}_#{img_name}.svg"
    img_path = "./public/#{file_path.to_s.split("/").last}"

    # Make sure there isn't going to be a file collision.
    if File.file?(result_img)
      system "rm #{result_img}"
    end

    # File.open("./public/svg/#{current_user.id}_#{img_file}", "wb") do |file|
    #   file.write( open("#{img}").read )
    # end
    #convert to black and white
    if params[:is_color] == 'false'
      system "convert #{img.path} ppm:- | mkbitmap -f 2 -s 2 -t 0.48 | potrace -t 5  -b svg -o #{result_img}"
    else
      ##############################################################
      ## Potentially better but not working currently.
      ##############################################################
      # system "convert -resize #{img.width * 2}x#{img.height * 2} #{img_path} #{img_path}"
      # system "python3 bin/color_trace/color_trace_multi.py -i #{img_path} -o #{result_img} -c 32 -S 1.334 -p 2 -O 2"
      ##############################################################
      ## End
      ##############################################################
      system "autotrace -output-file #{result_img} -output-format svg --color-count 32 --error-threshold 10 --despeckle-level 20 --despeckle-tightness 8.0 --line-threshold 0.01 --corner-surround 0 --filter-iterations 4 --line-reversion-threshold 0 #{img.path}"

      # system "autotrace -output-file #{result_img} -output-format svg --color-count 128 --despeckle-level 20 --despeckle-tightness 8.0 --line-threshold 0.01 --corner-surround 100 --filter-iterations 1 --line-reversion-threshold 100 #{img.path}"
      # system "convert #{img.path} -type grayscale #{img.path}"
    end

    svg = File.read(result_img)
    # svg = open("./public/svg/#{current_user.id}_#{img_name}.svg").read
    # svg_xml = Nokogiri::XML.parse(svg.to_s)

    svg = svg.to_s
    unless svg.include?("xmlns=")
      svg.gsub!('<svg', '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 400 400"')
    end

    unless params[:img].blank?
      system "rm #{file_path}"
    end

    system "rm ./public/svg/#{current_user.id}_#{img_name.to_s}.svg"

    # Unless there is no SVG in the file, save the logo.
    if svg.to_s.present?
      svg = SvgOptimizer.optimize(svg)

      user_logo = UserLogo.new

      user_logo.layout_template_id = nil
      user_logo.user_id = current_user.id
      user_logo.logo_id = nil
      user_logo.svg = svg.to_s
      user_logo.save

      render json: { success: true, svg: svg.to_s, id: user_logo.id }, status: 200
    else
      render json: { success: false }, status: 200
    end
  end

  def all_colors
    colors = PaletteColor.all

    render json: colors.to_json, status: 200
  end

  def all_palettes
    palettes = Palette.all

    render json: palettes.to_json, status: 200
  end

  def share_logo
    unless params[:logo_id].blank?
      if params[:type] === "share"
        shared_logo = SharedLogo.new
        user_logo = UserLogo.find(params[:logo_id])

        shared_logo.user_logo_id = "#{params[:logo_id]}"
        shared_logo.svg = user_logo.svg
        shared_logo.save
      else
        shared_logo = SharedLogo.find_by(user_logo_id: params[:logo_id])
        shared_logo.destroy
      end

      render json: { success: true }, status: 200
    end
  end

  def raster_to_svg
    img = MiniMagick::Image.open("#{params[:image_url]}")
    img.format "svg"

    # Create a new user_logo.
    user_logo = UserLogo.new

    user_logo.layout_template_id = nil
    user_logo.user_id = current_user.id
    user_logo.logo_id = nil
    user_logo.svg = img.to_blob.to_s
    user_logo.save

    render json: { success: true }, status: 200
  end

  def svg_to_psd
    img = params[:image_url]
    img_file = img.split("/").last
    img_name = File.basename(img_file,File.extname(img_file))

    system "rm ./public/#{img_name.to_s}.psd"

    system "convert #{img} ./public/#{img_name}.psd"

    render json: {url: "#{img_name.to_s}.psd", success: true}
  end

  def svg_to_raster
    logo = UserLogo.find(params[:logo_id])

    xml = Oga.parse_xml( logo.svg )

    logo_width = xml.xpath("//svg/@width").text
    logo_height = xml.xpath("//svg/@height").text

    svg_viewbox_node = xml.at_xpath("//svg/@viewbox")

    unless svg_viewbox_node.blank?
      if svg_viewbox_node.to_s != "0 0 #{logo_width} #{logo_height}"
        svg_viewbox_node.value = "0 0 #{logo_width} #{logo_height}"
        xml.to_xml
      end
    else
      svg = logo.svg.gsub("<svg", '<svg viewbox="0 0 #{logo_width} #{logo_height}"')

      xml = Oga.parse_xml( svg )
    end

    # Persist the SVG to a file so Inkscape can read it.
    File.open("#{Rails.root}/tmp/#{params[:logo_id]}-output.svg", "w") do |file|
      file.puts sanitize_svg(xml.to_xml.to_s)
      # file.puts sanitize_svg(logo.svg)
    end

    img = MiniMagick::Image.open("#{Rails.root}/tmp/#{params[:logo_id]}-output.svg")

    # Assign a size based on the size param.
    case params[:size]
      when "sm"
        size = 350
      when "md"
        size = 800
      when "lg"
        size = 2000
      when "og"
        size = nil
      when "cu"
        size = params[:cust]
    end

    unless size.nil?
      if img.height > img.width
        size_details = "-h #{size}"
      else
        size_details = "-w #{size}"
      end
    else
      size_details = nil
    end

    # Depending on whether we're in the dev environment or production, change how we call Inkscape.
    if Rails.env != 'production'
      inkscape_call = "/Applications/Inkscape.app/Contents/Resources/script"
    else
      inkscape_call = "inkscape"
    end

    # Run the Inkscape command from bash.
    if params[:file_type] != "jpg"
      `#{inkscape_call} \"#{Rails.root}/tmp/#{params[:logo_id]}-output.svg\" #{size_details} -e \"#{Rails.root}/tmp/#{params[:size]}-export-#{params[:logo_id]}.png\" --without-gui`
    else
      `#{inkscape_call} \"#{Rails.root}/tmp/#{params[:logo_id]}-output.svg\" #{size_details} -e \"#{Rails.root}/tmp/#{params[:size]}-export-#{params[:logo_id]}.png\" --export-background white --without-gui`
    end

    # Check if the file has been generated and when it has, send it as a download.
    25.times do
      if File.exists? "#{Rails.root}/tmp/#{params[:size]}-export-#{params[:logo_id]}.png"
      # if File.exists? "#{Rails.root}/tmp/#{params[:size]}-export-#{params[:logo_id]}.#{params[:file_type]}"

        img = MiniMagick::Image.open("#{Rails.root}/tmp/#{params[:size]}-export-#{params[:logo_id]}.png")
        # img = MiniMagick::Image.open("tmp/#{params[:size]}-export-#{params[:logo_id]}.#{params[:file_type]}")

        unless params[:file_type] === "png"
          img.format "#{params[:file_type]}"
          img.quality "100"
          # img.alpha "remove"

          img.write "#{Rails.root}/tmp/#{params[:size]}-export-#{params[:logo_id]}.#{params[:file_type]}"
        end

        # Remove the generated SVG file.
        `rm #{Rails.root}/tmp/#{params[:logo_id]}-output.svg`

        if params[:file_type] != "png"
          `rm #{Rails.root}/tmp/#{params[:size]}-export-#{params[:logo_id]}.#{params[:file_type]}`
        end

        # Exit the waiting loop.
        break
      else
        sleep(1)
      end
    end

    # Send the generated image as download.
    send_data img.to_blob, :disposition => 'attachment', :filename => "#{params[:size]}-export-#{params[:logo_id]}.#{params[:file_type]}"

    # `rm #{Rails.root}/tmp/#{params[:size]}-export-#{params[:logo_id]}.png`

    # unless params[:file_type] === "png"
      `rm #{Rails.root}/tmp/#{params[:size]}-export-#{params[:logo_id]}.#{params[:file_type]}`
    # end
    # send_data img.to_blob, :disposition => 'inline', :filename => "export.#{params[:file_type]}"
  end

  def logo_ocr
    img = params[:image_url]

    image = RTesseract.new("./app/assets/images/image_with_text.png", processor: "mini_magick")

    text = image.to_s

    render json: { success: true, scanned: text }
  end

  def save
    if params[:svg].present?
      # Update the SVG to contain the font.
      doc = Nokogiri::HTML.fragment(params[:svg])

      # Attempt to get the "style" tag.
      style_el = doc.at_css("style")

      # If the "style" tag exists, remove it.
      unless style_el.blank?
        doc.at_css("style").remove
      end

      unless params[:font_family].blank?
        font = get_font_style(params[:font_family])
        doc.at_css("svg").children.first.add_previous_sibling '<style type="text/css">' + font + '</style>'
      end

      text_elements = doc.css("text")

      if text_elements.present?
        text_elements.attr("stroke").remove if doc.css("text").attr("stroke")
        text_elements.attr("stroke-width").remove if doc.css("text").attr("stroke-width")
      end

      svg = doc.to_s

      # Remove the attributes that are causing rendering to fail.
      svg = svg.gsub('stroke="null"', "")
      svg = svg.gsub(' stroke-width="0"', "")
      svg = svg.gsub(/"'/, '"')
      svg = svg.gsub(/'"/, '"')

      user_logo = UserLogo.new

      user_logo.layout_template_id = 0
      user_logo.user_id = current_user.id
      user_logo.logo_id = params[:logo_id]
      user_logo.svg = svg.to_s
      user_logo.save

      render json: { success: true , user_logo_id: user_logo.id}, status: 200
    else
      render json: { success: false }
    end
  end

  def get_svg
    unless params[:logo_id].blank?
      logo = Logo.select(:svg).find(params[:logo_id])

      render json: { success: true, svg: "#{logo.svg}" }, status: 200
    else
      render json: { success: false }, status: 200
    end
  end

  def update_user_logo
    unless (params[:svg].blank? || params[:user_logo_id].blank?) || params[:user_logo_id] === "editor"
      # Update the SVG to contain the font.
      doc = Nokogiri::HTML.fragment(params[:svg])

      # Attempt to get the "style" tag.
      style_el = doc.at_css("style")

      # If the "style" tag exists, remove it.
      unless style_el.blank?
        doc.at_css("style").remove
      end

      # Get all font families.
      unless params[:font_family].blank?
        font = get_font_style(params[:font_family])
        doc.at_css("svg").children.first.add_previous_sibling '<style type="text/css">' + font + '</style>'
        # font_families = params[:font_family][0].split(", ")
        #
        # font = get_font_style(params[:font_family])
        # doc.at_css("svg").children.first.add_previous_sibling "<style type=\"text/css\">#{font}</style>"
      end

      doc.at_css("text").attr("stroke").remove if doc.at_css("text") && doc.at_css("text").attr("stroke")
      doc.at_css("text").attr("stroke-width").remove if doc.at_css("text") && doc.at_css("text").attr("stroke-width")

      svg = doc.to_s

      # Remove the attributes that are causing rendering to fail.
      svg = svg.gsub('stroke="null"', "")
      svg = svg.gsub(/"'/, '"')
      svg = svg.gsub(/'"/, '"')

      #################################################
      # Ensuring that the canvas size saved is correct.
      #################################################
      # Switching to Oga - will eventually switch across the entire app.
      xml = Oga.parse_xml( svg )

      logo_width = xml.xpath("//svg/@width").text
      logo_height = xml.xpath("//svg/@height").text

      svg_viewbox_node = xml.at_xpath("//svg/@viewbox")

      if !svg_viewbox_node.blank? && (svg_viewbox_node.to_s != "0 0 #{logo_width} #{logo_height}")
        svg_viewbox_node.value = "0 0 #{logo_width} #{logo_height}"
        svg = xml.to_xml.to_s
      end
      #################################################
      # End ensuring the canvas size saved is correct.
      #################################################

      user_logo = UserLogo.find(params[:user_logo_id])

      user_logo.svg = svg.to_s
      user_logo.save

      render json: { success: true }, status: 200
    else
      render json: { success: false }, status: 200
    end
  end

  def get_font_style(font_family)
    if font_family.class == Array
      import_array = []
      # font_face_array = []
      font_family.each do |el|
        # If we have multiple fonts, split this into an array.
        el = el.split(", ")

        # If we have an array, iterate. Otherwise, just get the results.
        if el.class == Array
          el.each do |e|
            # If the font-family is sans-serif, ignore it as web safe.
            if check_font_inclusion(e)
            # unless e === "sans-serif" || e === "Helvetica" || e === "Arial" || e === "Sans" || e.blank?
              font_hash = google_font_css(e)

              import_array << font_hash[:import]
              # font_face_array << font_hash[:font_face]
            end
          end
        else
          if check_font_inclusion(el)
          #unless el.blank?
            font_hash = google_font_css(el)

            import_array << font_hash[:import]
            # font_face_array << font_hash[:font_face]
          end
        end
      end
      %Q(#{import_array.join("")})
    else
      if check_font_inclusion(params[:font_family])
      #unless params[:font_family].blank?
        font_hash = google_font_css(params[:font_family])
        "#{font_hash[:import]};"
      end
    end
  end

  def delete_user_logo
    if params[:user_logo_id].present? && UserLogo.exists?(params[:user_logo_id])
      UserLogo.find(params[:user_logo_id]).try(:destroy)

      redirect_to logo_path(page: params[:page])
    else
      redirect_to logo_path
    end
  end

  def ajax_delete_user_logo
    unless params[:user_logo_id].blank?
      UserLogo.find(params[:user_logo_id]).try(:destroy) if params[:user_logo_id].present?

      render json: {success: true, message: "The selected logo has been deleted."}
    else
      render json: {success: false, message: "No logo has been selected."}
    end
  end

  def as_file
    unless params[:logo_id].blank?
      logo = Logo.find(params[:logo_id])

      # Sanitize the saved SVG.
      svg = sanitize_svg(logo.svg)

      #svg = logo.svg.gsub!("translate(-20,-20)","")
      #svg = svg.gsub!("viewBox=\"0 0 400 400\"", "viewBox=\"0 0 700 700\"")
      #svg = svg.gsub!("<svg", "<svg style=\"background-color: rgba(255,255,255,0);\"")

      respond_to do |format|
        format.svg {
          send_data svg, filename: "logo.svg", type: "image/svg+xml", disposition: "inline"
          # send_data logo.svg, filename: "logo.svg", type: "image/svg+xml", disposition: "inline"
        }
        format.all {
          send_data svg, filename: "logo.svg", type: "image/svg+xml", disposition: "inline"
          # send_data logo.svg, filename: "logo.svg", type: "image/svg+xml", disposition: "inline"
        }
      end
    end
  end

  def user_logo_as_file
    unless params[:logo_id].blank?
      logo = UserLogo.find(params[:logo_id])

      ############################################
      ## Edit the SVG to match our requirements ##
      ############################################
      svg_xml = Nokogiri::XML.parse(logo.svg.to_s)
      svg = logo.svg.to_s

      unless svg_xml.at_xpath("//xmlns:svg/@viewBox")
        svg.gsub!('xmlns="http://www.w3.org/2000/svg"', 'xmlns="http://www.w3.org/2000/svg" class="svgElement"')
        # svg.gsub!('xmlns="http://www.w3.org/2000/svg"', 'xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid meet" class="svgElement"')
        svg.gsub!('<style type="text/css">#{font}</style>', "")
      end

      ############################################
      ## End editing SVG.                       ##
      ############################################

      svg = sanitize_svg(svg)

      send_data svg, filename: "logo.svg", type: "image/svg+xml", disposition: "attachment"
      # send_data svg, filename: "logo.svg", type: "image/svg+xml", disposition: "inline"
    end
  end

  def shared_logo_as_file
    unless params[:logo_id].blank?
      logo = SharedLogo.find(params[:logo_id])

      send_data logo.svg, filename: "logo.svg", type: "image/svg+xml", disposition: "inline"
    end
  end

  def as_json
    unless params[:logo_id].blank?
      logo = Logo.find(params[:logo_id])

      render json: logo
    end
  end

  def categories_list
    @categories = Category.all.includes(:logos)

    #render json: @categories
  end

  def get_page
    unless params[:page_url].blank?
      page = Nokogiri::HTML(open("#{params[:page_url]}"))

      images = []

      page.xpath("//img").each do |img|
        img_uri = URI.parse( img["src"] )

        unless img_uri.scheme === "http" || img_uri.scheme === "https"
          images << URI.join( params[:page_url], img["src"] ).to_s
        else
          images << img["src"]
        end
      end

      render json: images.to_json, :status => 200
    else
      puts "The params didn't get through."
    end
  end

  def get_palettes
    @palettes = Palette.all.includes(:palette_colors)
  end

  def get_layouts
    @layouts = Layout.all

    render json: @layouts.to_json, status: 200
  end

  def google_font_css(font_name)
      unless font_name.blank?
        font_name_clear = font_name.gsub("'", "")

        # Pull the font data from the database.
        font = Font.find_by(name: "#{font_name_clear}")

        # Return a hash with the import and font_face (is the import needed?)
        { import: font.import }
      end
  end

  def check_font_inclusion(font_name)
    fonts = Font.all.pluck(:name)

    if fonts.include? font_name
      true
    else
      false
    end
  end
end
