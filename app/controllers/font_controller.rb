class FontController < ApplicationController

  private

  def font_params
    params.require(:font).permit(:name, :url, :import, :font_face)
  end
end