class WelcomeController < ApplicationController
  layout false

  skip_before_action :verify_authenticity_token

  def index

  end

  def zaxaa
    #API signature - FVZ2GCP5171TZY17
    respond_to do |format|
      #format.html {render :jvzoo_instant_notification, :layout => false}
      format.html do
        #logger.info "Returning #{params_str}"
        #render text: params_str
        render text: 'FVZ2GCP5171TZY17'
      end

      format.json do
        render json: {api_signature: 'FVZ2GCP5171TZY17'}, status: 200
      end

      format.js do
        render json: {api_signature: 'FVZ2GCP5171TZY17'}, status: 200
      end
    end
  end

  def zaxaa_zpn
    trans_type = params[:trans_type]
    hash_key = params[:hash_key]
    trans_gateway = params[:trans_gateway]
    trans_date = params[:trans_date]
    trans_testmode = params[:trans_testmode]
    seller_id = params[:seller_id]
    seller_email = params[:seller_email]
    cust_email = params[:cust_email]
    cust_firstname = params[:cust_firstname]
    cust_lastname = params[:cust_lastname]
    cust_address = params[:cust_address]
    cust_state = params[:cust_state]
    cust_city = params[:cust_city]
    cust_country = params[:cust_country]
    license_type = params[:license_type]

    # Custom variable send from Zaxaa.
    cust_bonus_upgrade = params[:bonus_id]


    # Trans types: SALE, BILL, RFND, CGBK, INSF, CANCEL-REBILL, UNCANCEL-REBILL
    pwd = SecureRandom.hex(8)

    # Spawn a new User object...
    usr = User.new
    usr.password = pwd
    usr.password_confirmation = pwd
    usr.email = cust_email
    usr.first_name = cust_firstname
    usr.last_name = cust_lastname

    if cust_bonus_upgrade == "pixxly-bonus_upgrade"
      usr.bonus = true
    end

    usr.save

    # processor, trans, email, name, pwd, user_id, processor_id, subscription_id, partner_id
    #User.create!({:email => "#{ccustemail}", :password => "#{pwd}", :password_confirmation => "#{pwd}" })
    #notification_processor("zaxaa", "#{trans_type}", "#{cust_email}", "#{cust_firstname} #{cust_lastname}", "#{pwd}", usr.id, nil, nil, nil)

    @email = cust_email
    @password = pwd

    ses_messages(@email, @password)
    # AccountMailer.jv_join_welcome(cust_email, pwd).deliver

    # Build a response string.
    params_str = ""
    params.each do |p|
      params_str.concat("#{p} | ")
    end

    params_str.concat("6Pa0wh3qjq3_f_4")

    respond_to do |format|
      #format.html {render :jvzoo_instant_notification, :layout => false}
      format.html do
        logger.info "Returning #{params_str}"
        #render text: params_str, status: 200
        render text: 'FVZ2GCP5171TZY17', status: 200
      end
    end
  end

  def jvzoo
    logger.info "Received POST from JVZoo."
    ccustname = params[:ccustname]
    logger.info "ccustname = #{ccustname}."
    #ccuststate = params[:ccuststate]
    #ccustcc = params[:ccustcc]
    ccustemail = params[:ccustemail]
    logger.info "ccustemail = #{ccustemail}."
    #cproditem = params[:cproditem]
    #cprodtitle = params[:cprodtitle]
    #cprodtype = params[:cprodtype]
    ctransaction = params[:ctransaction]
    logger.info "ctransaction = #{ctransaction}."
    #ctransaffiliate = params[:ctransaffiliate]
    #ctransamount = params[:ctransamount]
    #ctranspaymentmethod = params[:ctranspaymentmethod]
    #ctransvendor = params[:ctransvendor]
    #ctransreceipt = params[:ctransreceipt]
    #cupsellreceipt = params[:cupsellreceipt]
    #caffitid = params[:caffitid]
    #cvendthru = params[:cvendthru]
    #cverify = params[:cverify]
    #ctranstime = params[:ctranstime]

    # Trans types: SALE, BILL, RFND, CGBK, INSF, CANCEL-REBILL, UNCANCEL-REBILL
    pwd = SecureRandom.hex(8)

    # Break the customer name into first and last.
    cust_name = ccustname.split(" ")
    first_name = cust_name[0]

    # Get rid of the first element in the cust_name array.
    cust_name.shift

    surname = cust_name.join(" ")

    # Spawn a new User object...
    usr = User.new
    usr.password = pwd
    usr.password_confirmation = pwd
    usr.email = ccustemail
    usr.first_name = first_name
    usr.last_name = surname
    usr.save

    @email = ccustemail
    @password = pwd

    ses_messages(@email, @password)

    # Build a response string.
    params_str = ""
    params.each do |p|
      params_str.concat("#{p} | ")
    end

    params_str.concat("6Pa0wh3qjq3_f_4")

    respond_to do |format|
      #format.html {render :jvzoo_instant_notification, :layout => false}
      format.html do
        logger.info "Returning #{params_str}"
        render text: params_str
      end
    end
  end

  def ses_messages(to, password)
    unless to.blank?
      begin
        ses = AWS::SES::Base.new(
            :access_key_id     => "AKIAIFLK45XNMCED525A",
            :secret_access_key => "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"
        )

        result = ses.send_email(
            :to        => ["#{to}"],
            :source    => "\"The Pixxly Team\" <noreply@pixxly.net>",
            :subject   => "Welcome to Pixxly!",
            :html_body => "This is a test email."
        )

        #result_hash = Hash.from_xml(result)

        return true
      rescue => e
        return false
      end
    end
  end
end