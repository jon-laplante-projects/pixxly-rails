class Users::RegistrationsController < Devise::RegistrationsController
  layout "user_layout"

  private

  def sign_up_params
    params.require(:user).permit(:first_name, :last_name, :frozen, :closed, :email, :password, :password_confirmation)
  end

  def account_update_params
    params.require(:user).permit(:first_name, :last_name, :frozen, :closed, :email, :password, :password_confirmation, :current_password)
  end
end