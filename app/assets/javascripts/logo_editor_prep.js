function update_layouts(svg, is_ie){
    var clean_svg;

    if (svg !== null) {
        clean_svg = svg;

        if (is_ie === true) {
            clean_svg = remove_svg_attributes(clean_svg);
        }

        $("#layout_palette_symbol").html( clean_svg );

        //Pixxly.Layouts.current.svg = clean_svg;
        //Pixxly.Palettes.current.svg = clean_svg;
        //Pixxly.Logos.current.svg = clean_svg;
    } else {
        $("#layout_palette_symbol").html("");
    }
}

function update_editor(svg, is_ie){
    // Set the symbol to the editor.
    var svgEdit = window.frames[0];

    if (svg !== null && is_ie === true) {
        // Let's stringify the HTML to accomodate IE/Edge.
        var clean_svg = remove_svg_attributes(svg);
    } else {
        var clean_svg = svg;
    }

    // Push the SVG from the layout to the editor.
    Pixxly.Layouts.current.svg = clean_svg;
    Pixxly.Palettes.current.svg = clean_svg;

    svgEdit.svgCanvas.setSvgString( clean_svg );
}

function remove_svg_attributes(svg){
    // Ugly, but the fastest way to deal with IE injecting rogue, breaking namespaces in SVG.
    // Get an XML doc from the string.
    var t = $.parseXML(svg);

    // Set up the SVG and strip rogue namespaces, if we're in Internet Explorer.
    if (is_ie === true) {
        // Map the attributes of the SVG xml, and remove them (primarily for IE.)
        $("svg", t).each(function(){
            var attributes = $.map(this.attributes, function(item){
                return item.name;
            });

            // Find each attribute of the svg tag, and remove it.
            var attr = $(this);
            $.each(attributes, function(i, item) {
                attr.removeAttr(item);
            });
        });

        // Apply the application standard dimensions to the svg.
        $("svg", t).attr("viewBox", "0 0 400 400");
        $("svg", t).attr("width", "400");
        $("svg", t).attr("height", "400");
    }

    return new XMLSerializer().serializeToString(t);
}

function attr_transform(svg_obj){
    // Get the first <g/>, parse it, and generate a transform attr that works.
    intervalId = setInterval(function(){
        var firstG = svg_obj.find('g').get(0);

        if (firstG.getBBox() != null && firstG.getBBox() > -1) {
        // if (firstG) {
            var transform = newSize(firstG);
            if(transform) {
                clearInterval(intervalId);
            } else {
                return;
            }

            $(firstG).attr('transform', transform);
            minifyTransform(firstG);

            Pixxly.currentTransform = transform;
        }
    }, 50);
}