$(function(){
    $(document).on("click", "#launch_logo_import_modal", function(){
        $("#button_for_import_files").val("");
    });

    $(document).on("change", "#button_for_import_files", function(e) {
        // Display the filename to the user.
        var filename = $("#button_for_import_files").val().split("\\").slice(-1)[0];
        $("#bonus_path").val(filename);

        // Get the file...
        var selectedFile = $('#button_for_import_files').get(0).files[0];

        // Check for the various File API support.
        if (window.File && window.FileReader && window.FileList && window.Blob) {
            // File API supported, instantiate FileReader and read svg.
            $("#bonus_upload_progress_div").show();
            $("#bonus_upload_progress").css("width", 0);

            var reader = new FileReader();
            reader.onload = function(e){
                // Get the file contents...
                if (e.target.result.length > 0) {
                    $("#bonus_upload_progress").css("width", "20%");
                    // Build the data payload...
                    var payload = {
                        svg: e.target.result,
                        logo_id: null,
                        font_family: null,
                        user_logo_id: null
                    };

                    $.ajax({
                        url: "/logo/save",
                        type: "POST",
                        data: payload,
                        // dataType: "json",
                        async: false,
                        success: function (data) {
                            $("#bonus_upload_progress").css("width", "100%");

                            var notice = '<div class="alert alert-success notify-top-right" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close" style="margin: 0 0 0 10px;"><span aria-hidden="true">&times;</span></button><i class="fa fa-check-square-o"></i>&nbsp;Your logo has been uploaded!</div>';

                            $("body").append(notice);

                            window.location.href = "/logo";
                        },
                        error: function (data) {
                            var notice = '<div class="alert alert-danger notify-top-right" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close" style="margin: 0 0 0 10px;"><span aria-hidden="true">&times;</span></button><i class="fa fa-exclamation-triangle"></i>&nbsp;There was a problem importing your bonus logos.<br/>Please check your internet connection.</div>';

                            $("body").append(notice);

                            $("#bonus_upload_progress_div").hide();
                            $("#bonus_upload_progress").css("width", "0%");
                            // alert("There was a problem generating your logo!");
                        }
                    });
                } else {
                    alert("Sorry, there's nothing in that file.");
                }
                console.log(e.target.result);
            };

            reader.readAsText(selectedFile);

        } else {
            alert('The File APIs are not fully supported by your browser.');
        }
    });
});