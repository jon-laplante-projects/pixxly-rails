function launchIntoFullscreen(element) {
  if(element.requestFullscreen) {
    element.requestFullscreen();
  } else if(element.mozRequestFullScreen) {
    element.mozRequestFullScreen();
  } else if(element.webkitRequestFullscreen) {
    element.webkitRequestFullscreen();
  } else if(element.msRequestFullscreen) {
    element.msRequestFullscreen();
  }
}

function exitFullscreen() {
  if(document.exitFullscreen) {
    document.exitFullscreen();
  } else if(document.mozCancelFullScreen) {
    document.mozCancelFullScreen();
  } else if(document.webkitExitFullscreen) {
    document.webkitExitFullscreen();
  }
}


function fullButt(){
  var iframe = $($("iframe").get(0).contentDocument);
    $(".save-svg", iframe).click(function(){
      svgEdit = window.frames[0];
      var svg = svgCanvas.getSvgString();
      var font = svgCanvas.getFontFamily();

      var userLogoId = $('#logo_editor').data('user-logo-id');
      var url = userLogoId ? '/logo/update' : '/logo/save'
      var payload = { svg: svg, logo_id: $("#current_logo_id").val(), font_family: font, user_logo_id: userLogoId };

    $.ajax({
        url: url,
        type: "POST",
        data: payload,
        dataType: "json",
        async: true,
        success: function(data){
          window.location = "/logo";
        },
        error: function(data){
          alert("There was a problem saving your logo!");
        }
      });
    });

  $(".edit-svg", iframe).click(function(){
      var payload = logoPayload();
      $.ajax({
          url: "/logo/update",
          type: "POST",
          data: payload,
          dataType: "json",
          async: true,
          success: function (data) {
              if (data["success"] === true) {
                  // Turn on autosave.
                  Pixxly.Logos.current.initial_save = true;

                  var notice = '<div class="alert alert-success notify-top-right" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close" style="margin: 0 0 0 10px;"><span aria-hidden="true">&times;</span></button><i class="fa fa-check-square-o"></i>&nbsp;Your logo has been saved.</div>';

                  parent.$("body").append(notice);

                  // Determine whether to leave the page.
                  // if (action === "save_exit") {
                      window.location = "/logo";
                  // }

                  return false;
              } else {
                  var notice = '<div class="alert alert-danger notify-top-right" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close" style="margin: 0 0 0 10px;"><span aria-hidden="true">&times;</span></button><i class="fa fa-exclamation-triangle"></i>&nbsp;There was a problem saving your logo.<br/>Please check your internet connection.</div>';

                  parent.$("body").append(notice);
              }

          },
          error: function (data) {
              var notice = '<div class="alert alert-danger notify-top-right" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close" style="margin: 0 0 0 10px;"><span aria-hidden="true">&times;</span></button><i class="fa fa-exclamation-triangle"></i>&nbsp;There was a problem saving your logo.<br/>Please check your internet connection.</div>';

              parent.$("body").append(notice);

              console.log(data);
              alert("There was a problem saving your logo!");
          }
      });
  });
}

$(document).ready(function() {
  $('iframe').load(function() {
    fullButt();
  });
})
