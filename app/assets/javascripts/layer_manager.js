$(function(){
  window.LayerManager = {} || LayerManager;

  LayerManager.getLayersInIframe = function() {
      if(window.frames[0]){
      // if(window.frames[0].document){
          return $(window.frames[0].document).find('#svgcontent > g[style="pointer-events:all"] > *:not(title)').get().reverse();
      }
  };

  LayerManager.getIframe = function(){
    return $($("iframe").get(0).contentDocument);
  }

  LayerManager.init = function() {
    var self = this;

    if(self.ready) {
      self.rebuildLayers();
      $(self.getIframe()).on('DOMNodeInserted DOMNodeRemoved', '#svgcontent', function(e){
        setTimeout(function(){
          self.rebuildLayers();
        });
      });
      return;
    }

    self.layers = [];

    function activateLayer(layerIndex){
      var layer = self.layers[layerIndex];
      if(layer == null || layer.length == 0) {
        return;
      }

      var canvas = window.frames[0].svgCanvas;
      canvas.clearSelection();
      canvas.addToSelection([layer]);
    }

    function removeLayer(layerIndex){
      var layer = self.layers[layerIndex];
      window.frames[0].svgCanvas.clearSelection();
      layer.remove();
    }

    function getIconsForIndex(index) {
      var iconUp = '<i data-action="up" class="fa fa-arrow-up item-up"></i>';
      var iconDown = '<i data-action="down" class="fa fa-arrow-down item-down"></i>';
      var icons = '<i data-action="remove" class="fa fa-times item-remove"></i>';

      var countLayers = self.layers.length;

      if (index == 0) {
        icons = iconDown + icons;
      } else {
        if (index == countLayers - 1) {
          icons = iconUp + icons;
        } else {
          icons = iconDown + iconUp + icons;
        }
      }

      return icons;
    }

    function getLayerTypeOf(element) {
      if ($(element).is('g')) {
        if($(element).attr('id') == 'layer2'){
          return 'text';
        }
        return 'Picture';
      }

      return $(element).prop('tagName');
    }

    function getIconFor(type) {
      switch (type) {
        case 'Picture':
          return '<i class="fa fa-picture-o"></i>';
        case 'text':
          return '<i class="fa fa-bold"></i>';
        default:
          return '<i class="fa fa-square-o"></i>';
      }
    }

    function layerManagerBuild(){
      var layerManager = $('#layer_manager .list-group');
      layerManager.html('');
      self.layers = self.getLayersInIframe();

      $.each(self.layers, function(index, elem){
        var type = getLayerTypeOf(elem);
        var typeIcon = getIconFor(type);

        var icons = getIconsForIndex(index);
        layerManager.append('<div data-layer-index=' + index
          + ' class="list-group-item activate-layer-button">'
          + typeIcon + type + icons + '</div>');
      })

    }

    self.rebuildLayers = function(){
      layerManagerBuild();
    };

    $(document).on('click', '.activate-layer-button', function(){
      var layerIndex = $(this).data('layer-index');
      activateLayer(layerIndex);
    });

    $(document).on('click', 'i.item-up,i.item-down,i.item-remove', function(){
        var item = $(this);
        var activateLayerButton = item.closest('.activate-layer-button');
        var layerIndex = activateLayerButton.data('layer-index');
        var newLayerIdx = layerIndex;

        // Moved into a $.when().then() promise to ensure synchronicity.
        $.when(activateLayer(layerIndex)).then(function(){

            var iframe = self.getIframe();
            switch(item.attr('data-action')) {
              case 'up':
                iframe.find('#tool_move_up').trigger('click');
                newLayerIdx++;
                break;
              case 'down':
                iframe.find('#tool_move_down').trigger('click');
                newLayerIdx--;
                break;
              case 'remove':
                removeLayer(layerIndex);
                newLayerIdx = null;
                break;
            }

            // Rebuild the layers manager so that items are stacked as they should be.
            layerManagerBuild();
        });
    });

    layerManagerBuild();
    self.ready = true;
  };


  $('iframe#logo_editor').load(function(){
    var svgedit = window.frames[0].svgedit;
    window.frames[0].svgCanvas.ready(function(){
      LayerManager.init();
    });
  });
});

// Rebuild the layer manager so we pick up any changes made in the editor.
setInterval(function(){
    if(typeof LayerManager.rebuildLayers === 'function'){
        LayerManager.rebuildLayers();
    }
}, 1250);