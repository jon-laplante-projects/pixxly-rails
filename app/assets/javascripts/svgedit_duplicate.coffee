class SvgeditDuplicate
  MENU_SELECTOR: '#cmenu_canvas'

  constructor: (menuItemClass) ->
    @menuItemClass = menuItemClass || '.duplicate-item'
    @menuItemSelector = @MENU_SELECTOR + ' ' + @menuItemClass
    @svgCanvas = window.frames[0].svgCanvas
    @frameDocument = window.frames[0].document
    @setCallbacks()

  setCallbacks: ->
    $(@frameDocument).on 'mousedown', '#svgcanvas svg', => @checkIsAbleToDuplicate()
    $(@frameDocument).on 'click', @menuItemSelector, => @duplicateSelected()

  enableMenuItem: -> @getClosestLi().removeClass('disabled')
  disableMenuItem: -> @getClosestLi().addClass('disabled')

  isSomethingSelected: -> @svgCanvas.getSelectedElems().length > 0

  getClosestLi: -> @getMenuItem().closest('li')
  getMenuItem: -> $(@frameDocument).find(@menuItemSelector)

  checkIsAbleToDuplicate: ->
    if @isSomethingSelected()
      @enableMenuItem()
    else
      @disableMenuItem()

  duplicateSelected: -> @svgCanvas.cloneSelectedElements(20, 20)

window.initSvgeditDuplicate = -> new SvgeditDuplicate()
