/**
 * Created by Jon La Plante on 12/1/15.
 */
var Pixxly = Pixxly || {};
var svgEdit = svgEdit || window.frames[0];

Pixxly.setColorsForNodesInGroup = function (svgContent) {
    var moveColors = function () {
        $group = $(this);
        $group.find('>*').each(function () {
            var $node = $(this);

            $.each(['stroke', 'fill'], function (_, attribute) {
                if ($node.attr(attribute) === undefined) {
                    $node.attr(attribute, $group.attr(attribute));

                }
                if ($node.nodeName === 'g') moveColors($node);
            });
        });
    };

    $svg = $('<div><div/>').html(svgContent);
    $svg.find('g').each(moveColors);

    return $svg.html();
};

Pixxly.Logos = {
    getCategories: function () {
        $.getJSON("/logo/categories/list.json", function (data) {
            Pixxly.Logos.categories = data.categories;
        });
    },
    categories: {},
    current: {
        id: null,
        svg: null,
        working_svg: null,
        oca: false,
        oca_id: null,
        oca_search: null,
        oca_page: null,
        initial_save: false,
        canvas: { width: null, height: null }
    },
    generate: function () {

    }
};

Pixxly.Palettes = {
    init: function () {
        $.getJSON("/color/palettes.json", function (data) {
            Pixxly.Palettes.collection = data.palettes;
        });

        Pixxly.Palettes.current = {id: null, label: null, colors: [], color_map: []}
    },
    collection: [],
    current: {
        id: null,
        label: null,
        colors: [],
        color_map: []
    },
    code: null,
    switchPalettes: function (palette_obj) {
        // Set the currently selected palette.
        Pixxly.Palettes.current = $.grep(Pixxly.Palettes.collection, function (p) {
            return p.id == $(palette_obj).attr("data-id")
        })[0];
        Pixxly.Palettes.current.color_map = [];

        // Update the UI.
        $(".palette-option").removeClass("active");
        $(palette_obj).addClass("active");

        var fills = $("[fill]", Pixxly.Logos.current.svg);
        var temp_fills_arr = [];
        var fills_arr = [];
        var svg = Pixxly.Logos.current.svg;
        var oc = [];

        // Get all the used colors.
        $.each(fills, function (key, val) {
            temp_fills_arr.push($(val).attr("fill"));
        });

        // Remove duplicates from array.
        $.each(temp_fills_arr, function (i, el) {
            if ($.inArray(el, fills_arr) === -1) fills_arr.push(el);
        });

        oc = fills_arr;

        // Replace colors in logo.
        $.each(fills_arr, function (key, val) {
            if (key >= Pixxly.Palettes.current.colors.length) {
                key = key - Pixxly.Palettes.current.colors.length;
            }

            // Add the switched color mapping to the obj.
            // Grep the color mappings.
            var curr_color = [];
            if (Pixxly.Palettes.current.color_map.length > 0) {
                curr_color = $.grep(Pixxly.Palettes.current.color_map, function (c) {
                    return c.orig_color == val
                });
            }

            var color = Pixxly.Palettes.current.colors[key];
            svg = svg.replace(new RegExp(val, "g"), color.color);
            svg = svg.replace(new RegExp(String(val).toUpperCase(), "g"), String(color.color).toUpperCase());

            if (curr_color[0] != null) {
                curr_color[0]["new_color"] = color.color;
            } else {
                Pixxly.Palettes.current.color_map.push({orig_color: val, new_color: color.color});
            }
        });

        Pixxly.Palettes.code = svg;

        // Ensure the SVG size doesn't change
        attr_transform($(svg));

        // Get the active layout.
        var layout_obj = $(".layout-option.active");

        // Set the Palettes code for the layout rebuild.

        Pixxly.Palettes.code = svg;

        // Set the layout palette code.
        setTimeout(function () {
            ocaStandart();
        }, 200);

        $("#layout_palette_symbol").html(svg);

        // Rebuild the layout.
        $('#layer2').remove();
        Pixxly.Layouts.switchLayouts(layout_obj);

        // Show the layout palette.
        setTimeout(function () {
            $("#layout_palette_symbol").css({opacity: 0.0, visibility: "visible"}).animate({opacity: 1}, "fast");
        }, 250);

        $("#layout_palette_symbol").attr("data-id", Pixxly.Logos.current.id);

        // Set the symbol to the editor.
        svgEdit = window.frames[0];

        // Let's stringify the HTML to accomodate IE/Edge.
        var curr_svg = $("#layout_palette_symbol").prop('innerHTML');

        svgEdit.svgCanvas.setSvgString(curr_svg);
        $("#layout_palette_symbol").html(curr_svg);

        // Change the font color.
        Pixxly.PreviewCanvas.changeFontColor();

        updateColorChooser(oc);
    }
}

Pixxly.Layouts = {
    init: function () {
        $.getJSON("/layouts/list", function (data) {
            Pixxly.Layouts.collection = data;
        });
    },
    collection: [],
    current: {},
    switchLayouts: function (layout_obj) {
        var layout_id = $(layout_obj).data("id");

        // Set the currently selected palette.
        Pixxly.Layouts.current = $.extend(true, {}, $.grep(Pixxly.Layouts.collection, function (l) {
            return l.id == $(layout_obj).attr("data-id")
        })[0]);
        // Update the UI.
        $(".layout-option").removeClass("active");
        $(layout_obj).addClass("active");

        // Get a working copy of the edited logo.
        var svg = "";
        if (Pixxly.Palettes.code === null) {
            // if ( Pixxly.Palettes.code === null ) {
            svg = $("#layout_palette_symbol").prop("innerHTML");
        } else {
            svg = Pixxly.Palettes.code;
        }

        // Display the logo with layout.
        if (Pixxly.Layouts.current.id != 1) {
            // This XML DOCTYPE declaration can kill the SVG display, so let's remove it.

            if (svg.includes('<?xml version="1.0"?><!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">')) {
                svg = svg.replace('<?xml version="1.0"?><!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">', "");
            }

            // Store the current generic SVG.
            var layout = Pixxly.Layouts.current.svg;
            // var layout = Pixxly.Logos.current.svg;

            $("#temp_div").append(layout);
            $("#layout_palette_symbol svg").append($('#layer2'));

            // Change the font color.
            Pixxly.PreviewCanvas.changeFontColor();

            // var newX = $('text').attr('x');
            // var newY = $('text').attr('y');
            // Pixxly.Layouts.transform = 'translate(' + newX + ' ' + newY + ')';
            // $('#layer2 text').attr('transform', Pixxly.Layouts.transform);
            // $("#temp_div").html('');
        }

        // Pixxly.Logos.current.svg = ( svg );
        // curr_svg = svgAttributeSanitizer(svg);

        // Set the current logos.
        Pixxly.Layouts.current.svg = svg;
        Pixxly.Palettes.current.svg = svg;

        // Set the symbol to the editor.
        svgEdit = window.frames[0];
        svgEdit.svgCanvas.setSvgString(svg);
        //$("#layout_palette_symbol").html(svg);
    }
}

Pixxly.PreviewCanvas = {
    backgroundColor: "#B4B4B4",
    foregroundColor: "#000000",
    changeFontColor: function(){
        // Change the font color to go with the background color.
        $("svg", "#layout_palette_symbol").find("#layer2").css("fill", Pixxly.PreviewCanvas.foregroundColor);
        // $("svg", "#layout_palette_symbol").find("#layer2_text").css("fill", Pixxly.PreviewCanvas.foregroundColor);
    }
}

Pixxly.Helpers = {
    showPleaseWait: function (title) {

        var modal_title;
        if (title != null) {
            modal_title = title;
        } else {
            modal_title = "Loading symbols...";
        }

        $("#please_wait_title").text(modal_title);
        $("#pleaseWaitDialog").modal('show');
    },
    hidePleaseWait: function () {
        $("#pleaseWaitDialog").modal('hide');
    },
    currentLogoColor: null
}

// Integer test polyfill.
Number.isInteger = Number.isInteger || function isInteger (value) {
    return typeof value === 'number' &&
        isFinite(value) &&
        Math.floor(value) === value
}

function standardSize(str) {
    var g = $('#temp_g');
    if (g.length > 0) {
        gSize = g.get(0).getBBox();
    } else {
        gSize = {
            x: 0,
            y: 0,
            width: 400,
            height: 400
        }
    }

    var substr = str.match(/viewBox=".+"/g)[0].split('\" ')[0].split(" ");
    s = str;
    var coefX = gSize.width / parseInt(substr[substr.length - 1]);
    var coefY = gSize.height / parseInt(substr[substr.length - 1]);
    var x = parseInt(str.match(/ x=".+"/g)[0].split(' ')[1].split('=')[1].replace('\"', '')) * coefX;
    var y = parseInt(str.match(/ y=".+"/g)[0].split(' ')[1].split('=')[1].replace('\"', '')) * coefY;
    x = x - gSize.x * coefX;
    y = y - gSize.y * coefY;
    //var fontSize = parseInt(str.match(/font-size:.+;/g)[0].split(";")[0].split(":")[1].replace("px", '')) * coef;
    return str.replace(/viewBox=".+"/g, 'viewBox="0 0 400 400"')
        .replace(/ x=".+"/g, ' x="' + x + '"')
        .replace(/ y=".+"/g, ' y="' + y + '"')
}

function updateColorChooser(oc) {
    var html = '<div style="margin-bottom: 15px;"><small>' + Pixxly.Palettes.current.label + '</small></div><div style="display: flex; justify-content: center; justify-items: center;"><table style="width: 300px;" class="symbol-colors-table"><tr><td align="center" style="border-bottom: 1px solid silver; margin-right: 15px;">Original</td><td align="center" colspan="5" style="border-bottom: 1px solid silver;">Replacement</td></tr>';
    var color_rows = [];

    // Iterate the gray original colors.
    $.each(oc, function (key, val) {
        var row_open = '<tr><td align="center"><div class="palette-color original" data-color="' + val + '" style="background-color: ' + val + ';"></div></td>';

        // Iterate the palette colors and build the html table.
        $.each(Pixxly.Palettes.current.colors, function (k, v) {
            if (k === key) {
                color_rows.push('<td><div class="palette-color current active fa fa-check" data-color="' + v.color + '" style="background-color: ' + v.color + ';"></div></td>');
            } else {
                color_rows.push('<td><div class="palette-color current" data-color="' + v.color + '" style="background-color: ' + v.color + ';"></div></td>');
            }

        });

        html = html + row_open + color_rows.join("") + "</tr>"
        color_rows = [];
    });

    html = html + "</table></div>";

    // Set and show the palette color picker.
    $(".symbol-colors").html(html);
    $(".symbol-colors").show();
}

// Initializers.
Pixxly.Logos.getCategories();
Pixxly.Palettes.init();
Pixxly.Layouts.init();

// Change the canvas background color when creating a logo.
$(document).on("click", ".bg-color-change", function(){
    var bg_color = $(this).data("color");
    Pixxly.PreviewCanvas.backgroundColor = bg_color;

    $("#layout_palette_symbol").css("background-color", bg_color);

    // Change the font color to go with the background color.
    Pixxly.PreviewCanvas.changeFontColor();

    // Change the active bg border.
    $(".bg-color-change").removeClass("active");
    $(this).addClass("active");
});

// Change the canvas font color when creating a logo.
$(document).on("click", ".fg-color-change", function(){
    var fg_color = $(this).data("color");
    Pixxly.PreviewCanvas.foregroundColor = fg_color;

    // Change the font color to go with the background color.
    Pixxly.PreviewCanvas.changeFontColor();

    // Change the active bg border.
    $(".fg-color-change").removeClass("active");
    $(this).addClass("active");
});

// Handle logo category menu clicks.
$(document).on('click', '.logo-category-link', function () {

    // Show the loading modal.
    Pixxly.Helpers.showPleaseWait();

    // Set styles to reflect active category.
    $(".logo-category-link").removeClass("active");
    $(this).addClass("active");

    var category_id = parseInt($(this).attr("data-id"));

    $(".library-container").parent().parent().load("/logo/" + category_id + "/set", function () {
        Pixxly.Helpers.hidePleaseWait();
    });
});

// When creating a new logo, handle symbol click.
$(document).on("click", ".logo-symbol", function () {
    $(".logo-symbol-border-div").removeClass("active");
    $(".logo-symbol-border-div", this).addClass("active");

    $(".logo-symbol-selected-btn").attr("disabled", false);
    $("a.new-logo-layouts-tab").attr("data-toggle", "tab");
    // $("li.new-logo-layouts-tab").removeClass("disabled");
    // Get the SVG for the requested symbol.
    var id = $(this).attr("data-id");
    var payload = {logo_id: $(this).attr("data-id")};
    var svg_code;

    // showSmallNotification("Getting logo...");

    // Get the SVG from the server.
    $.ajax({
        url: "/logo/svg",
        type: "POST",
        data: payload,
        async: false,
        success: function (data) {
            if (data["success"] === true) {
                svg_code = data["svg"];
            } else {
                svg_code = null;
            }
        },
        error: function (data) {
            svg_code = null;
        }
    });

    Pixxly.Logos.current.id = id;

    // If there are separate comment and svg elements, use only the svg.
    if (typeof svg_code === "object" && svg_code != null) {
        svg_code = svg_code[1];
    }

    Pixxly.Logos.current.svg = svg_code;
    Pixxly.Layouts.current.svg = svg_code;
    Pixxly.Palettes.code = svg_code;

    $("#layout_palette_symbol").html(svg_code);
    $("#layout_palette_symbol").attr("data-id", id);

    // Persist the current logo id.
    $("#current_logo_id").val(id);
    Pixxly.Logos.current.oca = false;
});

var renderPayload = function (payload) {
    $("#images").html('');
    $.each(payload, function (i, item) {
        $("<a href='#' class='logo-symbols' data-id=" + item.id + "><div class='logo-symbol-border-div openclipartdiv' ><img class='openclipartimg' id='oca_" + item.id + "' src=" + item.svg.url + "></div></a>").appendTo("#images");
    });
};

var getImagesFromClipArt = function (params) {
    var api = "//openclipart.org/search/json/?";
    $.ajax({
        url: api,
        type: 'get',
        crossDomain: true,
        dataType: 'jsonp',
        data: {
            query: params.query,
            amount: 25,
            page: params.page,
            sort: 'downloads'
        },
        success: params.success
    });
};

function openClipArtQuery() {
    var tag = $("#search-i").val();
    getImagesFromClipArt({
        query: tag,
        success: function (data) {
            domPagination = $('#OpenClipArt_lib .images-pagination')
            if (domPagination.data('twbs-pagination')) {
                domPagination.twbsPagination('destroy');
            }

            domPagination.twbsPagination({
                totalPages: data.info.pages,
                visiblePages: 7,
                initiateStartPageClick: false,
                first: '« First',
                prev: '‹ Prev',
                next: 'Next ›',
                last: 'Last »',
                onPageClick: function (event, page) {
                    var query = $("#search-i").val();
                    getImagesFromClipArt({
                        query: query,
                        page: page,
                        success: function (data) {
                            renderPayload(data.payload);
                        }
                    });
                }
            });

            if (tag == '') {
                $('#OpenClipArt_lib .images-title').text('Most popular:');
            } else {
                $('#OpenClipArt_lib .images-title').text('Results for ' + tag);
            }

            renderPayload(data.payload);
        }
    });
}

// Handle clicking on file-format logo download buttons.
$(document).on("click", ".conversion_btn", function () {
    var format = $(this).attr("data-file-type");
    var id = $(this).attr("data-id");
    var size_arr = ["sm", "md", "lg", "og", "cu"];

    $(".download-alert-info").hide();

    // Download links.
    $.each(size_arr, function (key, val) {
        $('a.download-link[data-size="' + val + '"]').attr("href", "/export/raster/" + format + "/" + id + "/" + val);
    });
});

// Handle clicking of the download link.
$(document).on("click", ".download-link", function () {
    $(".download-alert-info").slideDown('fast');
});

$(document).on("click", ".open_clip_art_btn", function () {
    Pixxly.Helpers.showPleaseWait();
    openClipArtQuery();
    Pixxly.Helpers.hidePleaseWait();
});

// Handle click for the OpenClipArt selector.
$(document).on('click', '.openclipartdiv', function () {
    var img = $(this).find('img').get(0);
    $.ajax({
        url: img.src,
        type: "get",
        async: true,
        success: function (data) {
            /****************************/
            // Get the OCA image.
            /****************************/
            var svg_code = new XMLSerializer().serializeToString(data);

            // Cleanse the svg.


            $("#oca_img .logo-symbol-border-div").addClass("active");

            $(".logo-symbol-selected-btn").attr("disabled", false);
            $("a.new-logo-layouts-tab").attr("data-toggle", "tab");
            $("li.new-logo-layouts-tab").removeClass("disabled");
            var id = $(this).attr("data-id");

            $("script", svg_code).remove();
            // svg_test.css('script').remove
            $("title", svg_code).remove();
            // svg_test.xpath("//title").remove
            $("description", svg_code).remove();
            // svg_test.xpath("//description").remove
            $("li", svg_code).remove();
            // svg_test.xpath("//li").remove
            $("ul", svg_code).remove();
            // svg_test.xpath("//ul").remove

            svg_code = svg_sanitizer(svg_code);

            // Need to find ONLY single tick (') that has NOT been escaped.
            // svg_test.to_s.gsub(/\r?\n|\r\\n\r\\n/, '').gsub("'"){"\\'"}

            // Get the SVG for the requested symbol.
            Pixxly.Logos.current.id = id;
            Pixxly.Logos.current.svg = svg_code;
            Pixxly.Palettes.code = svg_code;

            $("#layout_palette_symbol").html(svg_code);
            $("#layout_palette_symbol").attr("data-id", id);

            // Persist the current logo id.
            // $("#current_logo_id").val('<%= @oca_id %>');
        },
        error: function (data) {
            alert("Sorry, we are unable to open the OpenClipArt image you've selected.");
        }
    });

    $(".openclipartimg").removeClass("active_oca");
    $(".openclipartdiv").removeClass("active");
    $(img).addClass("active_oca");
    $(img).parent().addClass("active");
    $(".logo-symbol-selected-btn").attr('disabled', false);

    Pixxly.Logos.current.oca = true;
});


// Handle double-click for the OpenClipArt selector.
//$(document).on("dblclick", ".openclipartdiv", function(){
//    if ($(this).attr('data-oca')) {
//        setTimeout(function(){
//            if ($("#temp_g").length == 0 && Pixxly.Logos.current.oca) {
//                ocaStandart();
//            }
//            loadToEditor();
//            // openEditor();
//        }, 600);
//    } else {
//        loadToEditor();
//    }
//
//    // Alter the layouts tab...
//    $("#palette_col").css("display", "none");
//    $("#symbol_col").removeClass("col-md-6");
//    $("#symbol_col").addClass("col-md-9");
//
//    $("#OpenClipArt_lib").modal("hide");
//});

$(document).on('shown.bs.modal', '#OpenClipArt_lib', function () {
    $(this).find(".logo-symbol-selected-btn").attr('disabled', 'disabled');
})

// Continue to step 2.
$(document).on('click', ".logo-symbol-selected-btn", function () {
    if ($(this).attr('data-oca')) {
        setTimeout(function () {
            if ($("#temp_g").length == 0 && Pixxly.Logos.current.oca) {
                ocaStandart();
            }
            loadToEditor();
            // openEditor();
        }, 700);

        // Record the relevant OCA data.
        Pixxly.Logos.current.oca = true;
        Pixxly.Logos.current.oca_id = $(".active_oca").parent().parent().data("id");
        Pixxly.Logos.current.oca_search = $('input[name="query"]').val();
        Pixxly.Logos.current.oca_page = parseInt($("div.images-pagination > ul.pagination > li.page.active > a").text());

        // Show the raster import link.
        $("#clone_oca_link_p").show();

        // Alter the layouts tab...
        $("#palette_col").css("display", "none");
        $("#symbol_col").removeClass("col-md-6");
        $("#symbol_col").addClass("col-md-9");

        // If we are on Edge, hide the layouts.
        if (browser === "Microsoft Edge") {
            $("#layouts_col").css("display", "none");
        }
    } else {
        // Remove OCA data.
        Pixxly.Logos.current.oca = false;
        Pixxly.Logos.current.oca_id = null;
        Pixxly.Logos.current.oca_search = null;
        Pixxly.Logos.current.oca_page = null;

        setTimeout(function () {
            ocaStandart();
        }, 200);
        loadToEditor();

        // Alter the layouts tab...
        $("#palette_col").css("display", "block");
        $("#layouts_col").css("display", "block");
        $("#symbol_col").removeClass("col-md-9");
        $("#symbol_col").addClass("col-md-6");

        // Hide the raster import link.
        $("#clone_oca_link_p").hide();
    }
    // Check and see if a layout is defined in the querystring.
    var qs = window.location.href.split("?")[1];
    var params = {};

    if (qs != null) {
        qs = qs.split("&");
        $.each(qs, function (key, val) {
            var key_val = val.split("=");
            var k = String(key_val[0]);
            var v = key_val[1];

            params[k] = v;
        });
    }

    // If we have a layout_id, choose the specified layout.
    if (params["la"] != null) {
        var layout_trigger = setInterval(function () {
            //setTimeout(function(){
            if ($("#layout_palette_symbol").html() != "" && $("#layout_palette_symbol").html() != null) {
                $('.layout-option[data-id="' + params["la"] + '"]').trigger("click");
                clearInterval(layout_trigger);
            }
        }, 250);
    }

    if (params["pl"] != null) {
        var palette_trigger = setInterval(function () {
            //setTimeout(function(){
            if ($("#layout_palette_symbol").html() != "" && $("#layout_palette_symbol").html() != null) {
                $('.palette-option[data-id="' + params["pl"] + '"]').trigger("click");
                clearInterval(palette_trigger);
            }
        }, 250);
    }

    $("#layout_tab").show();
    $("#symbol_tab").hide();
});

function loadToEditor() {
    svgEdit = window.frames[0];

    Pixxly.Helpers.showPleaseWait("Preparing editor...");
    if ($("#temp_g").length > 0) {
        standardLayoutEditor();
        $('#temp_g').append($('#layer2'));
        var resultHtml = $("#temp_g").html();
        $("#layout_palette_symbol svg").html(resultHtml);
        $("#temp_g").remove();
        loadSvgToEditor();
        setTimeout(function () {
            ocaEditor(Pixxly.currentTransform);
        }, 500)
    } else {
        standardLayoutEditor();
        // loadSvgToEditor();
    }
    var editor = $('#logo_editor').contents();
    if (editor.find('#layer2').length > 0) {
        $('#layer_0').trigger('click');
        editor.find('#tool_ungroup').trigger('click');
    }

    // Enable the "Layouts" tab.
    $('.new-logo-layouts-tab').parent().removeClass('disabled');
    $('.new-logo-layouts-tab').removeClass('disabled');
    $("#new_logo_nav a[href='#layout_tab']").tab("show");
    // $("#new_logo_nav a[href='#layout_tab']").tab("show");

    // Disable the "Symbols" tab.
    $("li.first-step").removeClass("active")
    $("li.first-step").addClass("disabled");
    $('[href="#symbol_tab"]').removeAttr("href");

    // Let's give it a short time to resize, etc.
    setTimeout(function () {
        Pixxly.Helpers.hidePleaseWait();
    }, 900);
}

function standardLayoutEditor() {
    if ($('#layer2').length > 0) {
        $('#layer2 text').attr('transform', Pixxly.Layouts.transform)
    }
}

function loadSvgToEditor() {
    var curr_svg = $("#layout_palette_symbol").prop("innerHTML");

    // This is ugly and hacky, but the best way to combat the IE namespace injections just now.
    // curr_svg = svgAttributeSanitizer(curr_svg);

    svgEdit.svgCanvas.setSvgString(curr_svg);
    // svgEdit.svgCanvas.setSvgString($("#layout_palette_symbol").html());
    Pixxly.Logos.current.svg = curr_svg;
    Pixxly.Layouts.current.svg = curr_svg;
    Pixxly.Palettes.code = curr_svg;
    $("#layout_palette_symbol").html(curr_svg);
}

// function openEditor() {
//     loadToEditor();
//
//     // Disable the "Layouts" tab.
//     $(".new-logo-layouts-tab").removeClass("active");
//     $(".new-logo-layouts-tab").addClass("disabled");
//     $('[href="#layout_tab"]').removeAttr("href");
//     $("#layout_tab").hide();
//
//
//     // Enable the "Editor" tab.
//     $("#new_logo_nav a[href='#editor_tab']").tab("show");
//     $(".new-logo-editor-tab").removeClass("disabled");
//     $("li.new-logo-editor-tab").addClass("active");
//     $("#editor_tab").removeClass("hidden-iframe-on-crutch");
//     // window.startLogoAutoSaving(true);
// }

// Return to step 1.
$(document).on('click', '.logo-chooser-btn', function () {
    // Disable the "Layouts" tab.
    $(".new-logo-layouts-tab").removeClass("active");
    $(".new-logo-layouts-tab").addClass("disabled");

    // Enable the "Symbols" tab.
    $(".first-step").removeClass("disabled");
    $(".first-step").addClass("active");
    $("a.first-step").tab("show");
    $(".new-logo-layouts-tab > a").attr("href", "#layout_tab");

    // Show/hide the tabs.
    $("#layout_tab").hide();
    $("#symbol_tab").show();

    // Hide palette color chooser.
    $(".symbol-colors").hide();
});

// Continue to step 3.
$(document).on('click', ".logo-defined-btn", function () {
    //// Detect FireFox.
    // if (typeof InstallTrigger !== 'undefined') {
    var logo_text_obj = $.grep(Pixxly.Layouts.collection, function(l){ return l.id == Pixxly.Layouts.current.id })[0];
    var logo_text_svg = $("layer2", logo_text_obj).prop("outerHTML");

    // Clear the temp layer2 from the logo.
    $("svg", "#layout_palette_symbol").remove("layer2");

    // Replace layer 2 in the logo.
    $("#layout_palette_symbol svg").append( logo_text_svg );

    // Pixxly.Logos.current.svg = $("#layout_palette_symbol").prop("innerHTML");
    svgEdit.svgCanvas.setSvgString($("#layout_palette_symbol").prop("innerHTML"));
    Pixxly.Logos.current.svg = svgEdit.svgCanvas.getSvgString();

    loadToEditor();

    // Now that the content has changed, reset it.
    Pixxly.Logos.current.svg = svgEdit.svgCanvas.getSvgString();

    var userLogoId = $('#logo_editor').data('user-logo-id');
    var payload = {
        svg: Pixxly.Logos.current.svg,
        // svg: svgEdit.svgCanvas.getSvgString(),
        logo_id: $("#current_logo_id").val(),
        font_family: null,
        user_logo_id: userLogoId
    };

    // Redirect params will stay empty if we aren't using a built-in symbol.
    var redirect_url_params = ""

    if ($("#current_logo_id").val() != "" && $("#current_logo_id").val() != null) {
        redirect_url_params = "?sy=" + $("#current_logo_id").val() + "&la=" + Pixxly.Layouts.current.id + "&pl=" + Pixxly.Palettes.current.id + "&pg=2";
    }

    $.ajax({
        url: "/logo/save",
        type: "POST",
        data: payload,
        // dataType: "json",
        async: false,
        success: function (data) {
            window.location.href = "/edit/" + data["user_logo_id"] + redirect_url_params;
            // window.location.href = "/edit/" + data["user_logo_id"];
        },
        error: function (data) {
            alert("There was a problem generating your logo!");
        }
    });
    //} else {
    //    // Set values.
    //    Pixxly.Logos.current.svg = $("#layout_palette_symbol").prop("innerHTML");
    //    svgEdit.svgCanvas.setSvgString( $("#layout_palette_symbol").prop("innerHTML") );
    //
    //    openEditor();
    //    LayerManager.init();
    //    initSvgeditDuplicate();
    //    // clearHistoryEditor(window.frames[0]);
    //}
});

// Return to step 2.
$(document).on('click', ".return-to-chooser-btn", function () {
    $(".new-logo-editor-tab").removeClass("active");
    $(".new-logo-layouts-tab > a").attr("href", "#layout_tab");
    $("#editor_tab").addClass("hidden-iframe-on-crutch");
    // window.stopLogoAutoSaving();

    $('.new-logo-layouts-tab').removeClass('disabled');
    $("#new_logo_nav a[href='#layout_tab']").tab("show");
    $(".new-logo-editor-tab").addClass("disabled");
    $("#layout_tab").show();
});

// Handle saving of the newly edited logo.
// $(document).on("click", ".save-svg", function(){
//     svgEdit = window.frames[0];
//     var svg = svgEdit.svgCanvas.getSvgString();
//     var font = svgEdit.svgCanvas.getFontFamily();

//     var payload = { svg: svg, logo_id: $("#current_logo_id").val(), font_family: font };

//     $.ajax({
//         url: "/logo/save",
//         type: "POST",
//         data: payload,
//         dataType: "json",
//         async: true,
//         success: function(data){
//             window.location = "/logo";
//         },
//         error: function(data){
//             alert("There was a problem saving your logo!");
//         }
//     });
// });

// Generate the payload for saving an edited logo.
function logoPayload(){
    svgEdit = window.frames[0];
    var svg = svgEdit.svgCanvas.getSvgString();
    var font = [];
    $('#logo_editor').contents().find('[font-family]').each(function (elem, index) {
        font.push($(this).attr('font-family'))
    })
    var url = window.location.href + '';
    url = url.split("/");
    var user_logo_id = url[url.length - 1];

    var payload = {svg: svg, user_logo_id: user_logo_id, font_family: font};

    return payload;
};

function saveAndExit(action){
    var alert_id = Math.floor(Math.random() * 26) + Date.now();
    var payload = logoPayload();
    if (action === "save_exit" || action === "save") {
        $.ajax({
            url: "/logo/update",
            type: "POST",
            data: payload,
            dataType: "json",
            async: true,
            success: function (data) {
                if (data["success"] === true) {
                    // Turn on autosave.
                    Pixxly.Logos.current.initial_save = true;

                    var notice = '<div class="alert alert-success notify-top-right" role="alert" id="' + alert_id + '"><button type="button" class="close" data-dismiss="alert" aria-label="Close" style="margin: 0 0 0 10px;"><span aria-hidden="true">&times;</span></button><i class="fa fa-check-square-o"></i>&nbsp;Your logo has been saved.</div>';

                    $("body").append(notice);

                    // Determine whether to leave the page.
                    if (action === "save_exit") {
                        window.location = "/logo";
                    }

                    return false;
                } else {
                    var notice = '<div class="alert alert-danger notify-top-right" role="alert" id="' + alert_id + '"><button type="button" class="close" data-dismiss="alert" aria-label="Close" style="margin: 0 0 0 10px;"><span aria-hidden="true">&times;</span></button><i class="fa fa-exclamation-triangle"></i>&nbsp;There was a problem saving your logo.<br/>Please check your internet connection.</div>';

                    $("body").append(notice);
                }

            },
            error: function (data) {
                var notice = '<div class="alert alert-danger notify-top-right" role="alert" id="' + alert_id + '"><button type="button" class="close" data-dismiss="alert" aria-label="Close" style="margin: 0 0 0 10px;"><span aria-hidden="true">&times;</span></button><i class="fa fa-exclamation-triangle"></i>&nbsp;There was a problem saving your logo.<br/>Please check your internet connection.</div>';

                $("body").append(notice);

                console.log(data);
                alert("There was a problem saving your logo!");
            }          
        }).done(function() {
            // If there is a notification, remove it after 3 seconds.
            setTimeout(function() {
              $("#" + alert_id).fadeOut( function(){
                $("#" + alert_id).remove();
              } );
            }, 3000);
          });
    } else {
        // No save, just a redirect.
        window.location = "/logo";
    }
};

// Exit without saving.
$(document).on("click", "#exit_without_saving", function(){
    // No save, just a redirect.
    window.location = "/logo";
});

// Save logo and exit.
$(document).on("click", "#save_logo_and_exit", function(){
    saveAndExit("save_exit");
})

// Handle updating of edited logo.
$(document).on("click", ".edit-svg", function () {
    saveAndExit("save");
});

// Exit and save editing.
$(document).on("click", ".exit-edit-svg", function(){
    if (Pixxly.Logos.current.initial_save === false) {
        $("#noSaveModal").modal("show");
    } else {
        saveAndExit("save_exit");
    }
});

// Save a logo from the shared library into the user library.
$(document).on("click", ".save-shared-svg", function () {
    var self = this;
    var alert_id = Math.floor(Math.random() * 26) + Date.now();
    var payload = {shared_id: $(this).data("id")};

    $.ajax({
        url: "/logo/shared/save",
        type: "POST",
        data: payload,
        async: false,
        success: function (data) {
            if (data["success"] === true) {
                var notice = '<div class="alert alert-success notify-top-right" role="alert" id="' + alert_id + '"><button type="button" class="close" data-dismiss="alert" aria-label="Close" style="margin: 0 0 0 10px;"><span aria-hidden="true">&times;</span></button><i class="fa fa-check-square-o"></i>&nbsp;Copied to your logos.</div>';

                $("body").append(notice);
            } else {
                var notice = '<div class="alert alert-danger notify-top-right" role="alert" id="' + alert_id + '"><button type="button" class="close" data-dismiss="alert" aria-label="Close" style="margin: 0 0 0 10px;"><span aria-hidden="true">&times;</span></button><i class="fa fa-exclamation-triangle"></i>&nbsp;Could not copy into your logos. Please check your internet connection.</div>';

                $("body").append(notice);
            }
        },
        error: function (data) {
            var notice = '<div class="alert alert-danger notify-top-right" role="alert" id="' + alert_id + '"><button type="button" class="close" data-dismiss="alert" aria-label="Close" style="margin: 0 0 0 10px;"><span aria-hidden="true">&times;</span></button><i class="fa fa-exclamation-triangle"></i>&nbsp;Could not copy into your logos. Please check your internet connection.</div>';

            $("body").append(notice);
        }
    }).done(function() {
      // If there is a notification, remove it after 3 seconds.
      setTimeout(function() {
        $("#" + alert_id).fadeOut( function(){
          $("#" + alert_id).remove();
        } );
      }, 3000);
    });
});

// Save a logo from the bonus library into the user library.
$(document).on("click", ".save-bonus-svg", function () {
    var self = this;
    var alert_id = Math.floor(Math.random() * 26) + Date.now();
    var payload = {bonus_id: $(this).data("id")};

    $.ajax({
        url: "/logo/bonus/save",
        type: "POST",
        data: payload,
        async: false,
        success: function (data) {
            if (data["success"] === true) {
                var notice = '<div class="alert alert-success notify-top-right" role="alert" id="' + alert_id + '"><button type="button" class="close" data-dismiss="alert" aria-label="Close" style="margin: 0 0 0 10px;"><span aria-hidden="true">&times;</span></button><i class="fa fa-check-square-o"></i>&nbsp;Copied to your logos.</div>';

                $("body").append(notice);
            } else {
                var notice = '<div class="alert alert-danger notify-top-right" role="alert" id="' + alert_id + '"><button type="button" class="close" data-dismiss="alert" aria-label="Close" style="margin: 0 0 0 10px;"><span aria-hidden="true">&times;</span></button><i class="fa fa-exclamation-triangle"></i>&nbsp;Could not copy into your logos. Please check your internet connection.</div>';

                $("body").append(notice);
            }
        },
        error: function (data) {
            var notice = '<div class="alert alert-danger notify-top-right" role="alert" id="' + alert_id + '"><button type="button" class="close" data-dismiss="alert" aria-label="Close" style="margin: 0 0 0 10px;"><span aria-hidden="true">&times;</span></button><i class="fa fa-exclamation-triangle"></i>&nbsp;Could not copy into your logos. Please check your internet connection.</div>';

            $("body").append(notice);
        }
    }).done(function() {
        // If there is a notification, remove it after 3 seconds.
        setTimeout(function() {
          $("#" + alert_id).fadeOut( function(){
            $("#" + alert_id).remove();
          } );
        }, 3000);
      });
});

// Handle duplication of user logos.
$(document).on("click", ".copy-logo-btn", function(){
    var alert_id = Math.floor(Math.random() * 26) + Date.now();
  
    // Let the user know we're working on it...
    var notice = '<div class="alert alert-info notify-top-right" role="alert" id="' + alert_id + '"><button type="button" class="close" data-dismiss="alert" aria-label="Close" style="margin: 0 0 0 10px;"><span aria-hidden="true">&times;</span></button><i class="fa fa-check-square-o"></i>&nbsp;Copying logo.</div>';

    var self = this;
    var payload = { logo_id: $(this).data("id") };
    
    $.ajax({
        url: "/logo/duplicate",
        type: "POST",
        data: payload,
        async: true,
        success: function(data){
            if ( data["success"] === true ) {
                // $("#my_logo").prepend( data["panel"] );

                var notice = '<div class="alert alert-success notify-top-right" role="alert" id="' + alert_id + '"><button type="button" class="close" data-dismiss="alert" aria-label="Close" style="margin: 0 0 0 10px;"><span aria-hidden="true">&times;</span></button><i class="fa fa-check-square-o"></i>&nbsp;Successfully copied.</div>';

                $("body").append(notice);

                location.reload();
            } else {
                var notice = '<div class="alert alert-danger notify-top-right" role="alert" id="' + alert_id + '"><button type="button" class="close" data-dismiss="alert" aria-label="Close" style="margin: 0 0 0 10px;"><span aria-hidden="true">&times;</span></button><i class="fa fa-exclamation-triangle"></i>&nbsp;There was a problem copying this logo. Please check your internet connection.</div>';

                $("body").append(notice);
            }
        },
        error: function(data){
            var notice = '<div class="alert alert-danger notify-top-right" role="alert" id="' + alert_id + '"><button type="button" class="close" data-dismiss="alert" aria-label="Close" style="margin: 0 0 0 10px;"><span aria-hidden="true">&times;</span></button><i class="fa fa-exclamation-triangle"></i>&nbsp;There was a problem copying this logo. Please check your internet connection.</div>';

            $("body").append(notice);
        }
    }).done(function() {
        // If there is a notification, remove it after 3 seconds.
        setTimeout(function() {
          $("#" + alert_id).fadeOut( function(){
            $("#" + alert_id).remove();
          } );
        }, 3000);
      });
});

// Handle launching the delete logo confirmation modal.
$(document).on("click", ".delete-logo-btn", function () {
    // Enable the confirmation button.
    $("#delete_logo_confirmation").attr("disabled", false);

    // Set the confirmation button logo id.
    $("#delete_logo_confirmation").attr("href", "/logo/delete?user_logo_id=" + $(this).attr("data-id") + '&page=' + $('.pagination li.active').children().text())
    // Show the modal.
    $("#deleteLogoModal").modal("show");
});

// Share or unshare the selected logo.
$(document).on("click", ".share-logo-btn", function () {
    var that = this;
    var payload, action;

    if ($(this).attr("data-status") === "true") {
        action = "unshare";
    } else {
        action = "share";
    }

    payload = {logo_id: $(this).attr("data-id"), type: action};

    $.ajax({
        url: "/logo/share",
        type: "POST",
        data: payload,
        async: true,
        success: function (data) {

            if (action === "unshare") {
                $(that).attr("data-status", false);
                $(that).html('<i class="fa fa-share-alt"></i>Share');
            } else {
                $(that).attr("data-status", true);
                $(that).html('<i class="fa fa-share-alt"></i>Unshare');

            }
        },
        error: function (data) {
            alert("Could not save.");
        }
    });
});

// Handle click in "Clones" to choose converstion to SVG or embedding image.
$(document).on("change", '[name="convert-or-embed"]', function () {
    if ($(this).val() === "import_svg") {
        $("#black_and_white_toggle").show();
        $("#convert_to_svg_checkbox").val(true);
    } else {
        $("#black_and_white_toggle").hide();
        $("#convert_to_svg_checkbox").val(false);
    }
});

// Convert files.
$(document).on("click", ".conversion-btn", function () {
    var id = $(this).attr("data-id");
    var filetype = $(this).attr("data-file-type");

    var payload = {logo_id: id, file_type: filetype};

    $.ajax({
        url: "/export/raster",
        type: "POST",
        data: payload,
        async: true,
        success: function (data) {

        },
        error: function (data) {

        }
    });
});

// Set the active color palette.
$(document).on("click", ".palette-option", function () {
    // Hide the layout palette so the flicker isn't happening.
    $("#layout_palette_symbol").css("visibility", "hidden");

    Pixxly.Palettes.switchPalettes(this);
});

// Handle color assignments from the currently selected palette.
$(document).on("click", ".palette-color.current", function () {
    // Set the layout palette code.
    setTimeout(function () {
        ocaStandart();
    }, 200);

    var container = $(this).parent().parent();
    var svg = Pixxly.Logos.current.svg;

    // Flag the currently selected replacement color.
    $(".palette-color.current", container).removeClass("active fa fa-check");
    $(this).addClass("active fa fa-check");

    // Replace colors in logo.
    $.each($(".symbol-colors-table tr"), function (key, val) {
        if (key != 0) {
            var oc = $(".original", val).attr("data-color");
            var nc = $(".palette-color.current.active", val).attr("data-color");

            // Get the color set from the current color map.
            var colors = $.grep(Pixxly.Palettes.current.color_map, function (c) {
                return c.orig_color == oc
            });

            // Map the newly chosen color into the color map.
            colors[0]["new_color"] = nc;

            svg = svg.replace(new RegExp(oc, "g"), nc);
            svg = svg.replace(new RegExp(String(oc).toUpperCase(), "g"), String(nc).toUpperCase());
        }
    });

    // Ensure the SVG size doesn't change
    svg_obj = $(svg);
    attr_transform($(svg));
    // Get the first <g/>, parse it, and generate a transform attr that works.
    //intervalId = setInterval(function(){
    //    var firstG = svg_obj.find('g').get(0);
    //
    //    if (firstG) {
    //        var transform = newSize(firstG);
    //        if(transform) {
    //            clearInterval(intervalId);
    //        } else {
    //            return;
    //        }
    //
    //        $(firstG).attr('transform', transform);
    //        minifyTransform(firstG);
    //
    //        Pixxly.currentTransform = transform;
    //        // canvasSize(editor);
    //    }
    //}, 50);
    //
    //var curr_svg = svgAttributeSanitizer( svg );
    //Pixxly.Palettes.code = curr_svg;

    // Get the active layout.
    var layout_obj = $(".layout-option.active");

    // Show the layout palette.
    $("#layout_palette_symbol").css("visibility", "hidden");

    // Set the Palettes code for the layout rebuild.
    Pixxly.Palettes.code = svg;
    $("#layout_palette_symbol").html(svg);
    // Pixxly.Palettes.code = $(svg).prop("outerHTML");

    // Rebuild the layout.
    $('#layer2').remove();
    Pixxly.Layouts.switchLayouts(layout_obj);

    // Fade in the symbol palettte and the palette color chooser.
    setTimeout(function () {
        $("#layout_palette_symbol").css({opacity: 0.0, visibility: "visible"}).animate({opacity: 1}, "fast");
    }, 250);

    $("#layout_palette_symbol").attr("data-id", Pixxly.Logos.current.id);

    // Set the symbol to the editor.
    svgEdit = window.frames[0];
    svgEdit.svgCanvas.setSvgString(svg);
});

// Set the active layout.
$(document).on("click", ".layout-option", function () {
    $('#layer2').remove();
    Pixxly.Layouts.switchLayouts(this);
});

function svgCenterAction(obj) {
    var doc = obj.contentDocument;
    $(doc).find('svg').on('click', function () {
        $(obj).parents('a').trigger('click');
    });

    var group = $(doc).find('g').get(0);

    if (group) {
        var values = group.getBBox();
        if (values.width == 0) {
            setTimeout(function () {
                values = group.getBBox();
                changeSize(group, values);
            }, 200);
        } else {
            changeSize(group, values);
        }
    }
}

function svgStandard() {
    $('object.svg').each(function (index, elem) {
        elem.addEventListener('load', function () {
            if (elem)
                svgCenterAction(elem);
        });
    })
}

// Capture carriage return in search box and submit it.
$("#search-i").keyup(function (e) {
    var keyCode = e.keyCode || e.which;

    if (keyCode == 13) {
        openClipArtQuery();
        return false;
    }
});

function ocaEditor(transform) {
    var editor = $('#logo_editor').contents();
    editor.find('#svgcanvas').attr('style', 'position: relative; width: 400px; height: 400px;')

    var svgs = editor.find('#svgcanvas svg');
    svgs.attr('width', 400);
    svgs.attr('height', 400);
    svgs.attr('x', 0);
    svgs.attr('y', 0);
    svgs.removeAttr('viewBox');
    svgs.attr('viewBox', '0 0 400 400');
    var firstG = $('#logo_editor').contents().find('g[style="pointer-events:all"]').get(0);
    $(firstG).children(':not(#layer2)').each(function () {
        var sourceT = $(this).attr('transform') || '';
        $(this).attr('transform', transform + sourceT);
        minifyTransform(this);
    });
    editor.find('#selectorParentGroup').attr('transform', '');
    $(firstG).attr('edited', true);
    editor.find('#canvas_height').val(400);
    editor.find('#canvas_width').val(400);
    clearHistoryEditor(window.frames[0]);

}

function clearHistoryEditor(svgedit) {
    svgedit.svgCanvas.undoMgr.resetUndoStack();
    $('#logo_editor').contents().find('#tool_undo').addClass('disabled')
}

function ocaStandart() {
    $('#temp_div').html('');
    var editor = $('#layout_palette_symbol');
    editor.html(svg_sanitizer(editor.html()));
    var svg = $(editor.find('svg').get(0));
    svg.attr('width', 400);
    svg.attr('height', 400);
    svg.attr('x', 0);
    svg.attr('y', 0);
    svg.removeAttr('viewBox');
    svg.removeAttr('viewbox');
    svg.attr('viewBox', '0 0 400 400');

    if (svg.find('g').length == 0) {
        var sourceSvg = svg.html();
        setTimeout(function () {
            svg.html('<g id="temp_g">' + sourceSvg + '</g>');
        }, 200);
    }

    intervalId = setInterval(function () {
        var firstG = svg.find('g').get(0);
        if (firstG) {
            var transform = newSize(firstG);
            if (transform) {
                clearInterval(intervalId);
            } else {
                return;
            }

            $(firstG).attr('transform', transform);
            minifyTransform(firstG);

            Pixxly.currentTransform = transform;
            canvasSize(editor);
        }
    }, 150)

}

function newSize(firstG) {
    var firstGSize = firstG.getBBox();
    if (firstGSize.width == 0) {
        return null;
    }

    var heightScreen = 400;
    var widthtScreen = 400;
    var commonScale = Math.min((widthtScreen - 100) / firstGSize.width, (heightScreen - 100) / firstGSize.height);

    var padX = (widthtScreen - firstGSize.width * commonScale) / 2;
    var padY = (heightScreen - firstGSize.height * commonScale) / 2;

    var offsetX = padX / commonScale - firstGSize.x;
    var offsetY = padY / commonScale - firstGSize.y;

    return ' scale(' + commonScale + ' ' + commonScale + ')translate(' + offsetX + ' ' + offsetY + ') ';
}

function minifyTransform(svgObject) {
    var dom = $(svgObject).get(0);
    var svgEdit = window.frames[0];
    var transformList = svgEdit.svgedit.transformlist.getTransformList(dom);
    if (transformList == null) {
        return;
    }
    var oneTransformation = svgEdit.svgedit.math.transformListToTransform(transformList);
    transformList.clear();
    transformList.appendItem(oneTransformation);
}

function canvasSize(editor) {
    editor.find('#selectorParentGroup').attr('transform', '');
    editor.find('#canvas_height').val(400);
    editor.find('#canvas_width').val(400);
}

function multiMatrix(firstM, secondM) {
    var a = firstM.replace(/["'()]/g, "").split(' ');
    var b = secondM.replace(/["'()]/g, "").split(' ');
    var newA = a[0] * b[0] + a[2] * b[1];
    var newB = a[1] * b[0] + a[3] * b[1];
    var newC = a[0] * b[2] + a[2] * b[3];
    var newD = a[1] * b[2] + a[3] * b[3];
    var newE = a[0] * b[4] + a[2] * b[5] + +a[4];
    var newF = a[1] * b[4] + a[3] * b[5] + +a[5];
    return '(' + newA + ' ' + newB + ' ' + newC + ' ' + newD + ' ' + newE + ' ' + newF + ')';
}

function changeSize(group, values) {
    var heightScreen = 400;
    var widthtScreen = 400;

    var padX = (widthtScreen - values.width * 4) / 2;
    var padY = (heightScreen - values.height * 4) / 2;

    var scaleX = (widthtScreen - 2 * padX) / values.width;

    var scaleY = (widthtScreen - 2 * padY) / values.height;

    var offsetX = padX / scaleX - values.x;
    var offsetY = padY / scaleY - values.y;

    $(group).attr('transform', 'scale(' + scaleX + ',' + scaleX + ')translate(' + offsetX + ',' + offsetY + ')');
}

svgStandard();

$(document).on("page:change", function () {
    svgStandard();
    logoCenter();
})

function logoCenter() {
    $('.my-logo svg, .library-container svg').each(function (index, elem) {
        var width = $(elem).attr('width');

        var height = $(elem).attr('height');

        if (width.indexOf('pt')) {
            width = ptToPx(width);
        }
        $(elem).attr('width', 326)
        if (height.indexOf('pt')) {
            height = ptToPx(height);
        }
        $(elem).attr('height', 395)
        elem.setAttribute('viewBox', '0 0 ' + width + ' ' + height);
    })

}


function ptToPx(str) {
    return str.replace(/([0-9]+)pt/g, function (match, group0) {
        return Math.round(parseInt(group0, 10) * 96 / 72);
    });
}

//$(document).on('click', '#new_logo_nav li.new-logo-layouts-tab', function(){
//    $(".logo-symbol-selected-btn").trigger('click');
//});

$(document).on('change', '#button_for_open_file', function () {
    $('#page_path').val($(this).get(0).files[0].name);
    $('#page_path').trigger('change');
});


$(document).on("page:restore", function () {
    $('.my-logo').load(location.href + " #my_logo", function () {
        logoCenter();
    });
})

$(document).on("click", ".font-container", function () {
    var new_font = $(".logo-font", this).text();

    // Set the font-family in the dropdown.
    $("#font_family_dropdown").val(new_font);

    // Set the dropdown window to display the new font.
    $("#font_family").val(new_font);
    $("#preview_font").css("font-family", new_font);
    $("#preview_font").html(new_font);
    $("select#font_family_dropdown").trigger("change");

    // Set the selected text to the new font.
    svgEdit = window.frames[0];
    svgEdit.svgCanvas.setFontFamily(new_font);

    // Set font-manager styles.
    $(".font-container").removeClass("active");
    $(this).addClass("active");
});

// If the user is going back to the layouts and palettes tab, delete the current logo.
$(document).on("click", ".return-to-layouts-btn", function () {
    var payload = {user_logo_id: $(this).data("user-logo-id")};

    // If we're going back to the first tab, remove the 2nd tab querystring data.
    if ($(this).hasClass("first-step")) {
        var params = window.location.href.split("?")[1].replace("&pg=2", "");
    } else {
        var params = window.location.href.split("?")[1];
    }

    // Delete the currently edited logo.
    $.ajax({
        url: "/logo/delete",
        type: "POST",
        data: payload,
        success: function (data) {
            if (data.success === true) {
                // Clear out the current logo id and replace it with what we're using.
                var qs_params = {};
                $.each(params.split("&"), function (key, val) {
                    var temp = val.split("=");
                    qs_params[temp[0]] = temp[1];
                });

                // Change current logo from the former user_logo_id to the logo_id.
                $("#current_logo_id").val(qs_params["sy"]);

                // Show the "loading" modal.
                $("#pleaseWaitDialog").modal("show");
                window.location.href = "/logo/new?" + params;
            } else {
                alert("Crap.");
            }

        },
        error: function (data) {
            alert("Oh, snap!");
        }
    });
});

// Listen for font-change in the editor and change it in the manager as well.
function setFontManager(font) {
    // Set font-manager styles.
    console.log("Font: " + font);
    $(".font-container").removeClass("active");
    $('[data-font-face="' + font + '"]').addClass("active");
}

// Things that must be handled after the DOM is loaded.
$(function(){
    $(document).on("change blur", "#logo_long_side", function(){
        var href = $('a[data-size="cu"]').attr("href");
        href = href.split("?")[0] + "?cust=" + $(this).val();
        $('a[data-size="cu"]').attr("href", href);
    })
});