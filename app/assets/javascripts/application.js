// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require bootstrap-sprockets
//= require twbs-pagination
// require bootstrap.colorpickersliders.min
// require bootstrap.colorpickersliders.nocielch.min
//= require logo
//= require logo_size
//= require fullscreen
//= require clone_oca
//= require svg_sanitizer
//= require svgedit_duplicate
//= require clone_cleanup
//= require_tree .

// Polyfill for string.prototype.includes...
if (!String.prototype.includes) {
    String.prototype.includes = function(search, start) {
        'use strict';
        if (typeof start !== 'number') {
            start = 0;
        }

        if (start + search.length > this.length) {
            return false;
        } else {
            return this.indexOf(search, start) !== -1;
        }
    };
}

function svgAttributeSanitizer(svg){
    svg = svg.replace('xmlns:xml="http://www.w3.org/XML/1998/namespace"', "");
    svg = svg.replace(/xmlns:NS1=""/g, '');
    svg = svg.replace(/xmlns:NS2=""/g, '');
    svg = svg.replace(/xmlns:NS3=""/g, '');
    svg = svg.replace(/NS1:ns3:xmlns:i="@ns_ai;"/g, '');
    svg = svg.replace(/NS1:ns3:ns1:xmlns:x="&amp;ns_extend;"/g, '');
    svg = svg.replace(/NS2:ns2:xmlns:graph="@ns_graphs;"/g, '');
    svg = svg.replace(/NS2:ns2:ns2:xmlns:graph="&amp;ns_graphs;"/g, '');
    svg = svg.replace(/NS3:ns1:xmlns:x="@ns_extend;"/g, '');
    svg = svg.replace(/NS3:ns1:ns3:xmlns:i="&amp;ns_ai;"/g, '');
    svg = svg.replace(/undefined=""/g, '');

    return svg;
}

// Calls after DOM is loaded...
$(function(){
    $("#trace_and_download").click(function(){
        payload = { image_url: $("#raster_image").val() };

        $.ajax({
            url: "raster_to_svg",
            type: "GET",
            data: payload,
            success: function(data){
                $("#raster_to_vector").append('<a href="/' + data.url + '" target="_BLANK">Download Results</a>');
            },
            error: function(data){

            }
        });
    });

    $("#download_jpg").click(function(){
        payload = { image_url: $("#svg_image").val(), file_type: "jpg" };

        $.ajax({
            url: "svg_to_raster",
            type: "GET",
            data: payload,
            success: function(data){
                $("#svg_to_jpg").append('<a href="/' + data.url + '" target="_BLANK">Download Results</a>');
            },
            error: function(data){

            }
        });
    });

    $("#download_psd").click(function(){
        payload = { image_url: $("#svg_for_psd").val(), file_type: "psd" };

        $.ajax({
            url: "svg_to_raster",
            type: "GET",
            data: payload,
            success: function(data){
                $("#svg_to_psd").append('<a href="/' + data.url + '" target="_BLANK">Download Results</a>');
            },
            error: function(data){

            }
        });
    });

    $("#download_gif").click(function(){
        payload = { image_url: $("#svg_for_gif").val(), file_type: "gif" };

        $.ajax({
            url: "svg_to_raster",
            type: "GET",
            data: payload,
            success: function(data){
                $("#svg_to_gif").append('<a href="/' + data.url + '" target="_BLANK">Download Results</a>');
            },
            error: function(data){

            }
        });
    });

    $("#download_png").click(function(){
        payload = { image_url: $("#svg_for_png").val(), file_type: "png" };

        $.ajax({
            url: "svg_to_raster",
            type: "GET",
            data: payload,
            success: function(data){
                $("#svg_to_png").append('<a href="/' + data.url + '" target="_BLANK">Download Results</a>');
            },
            error: function(data){

            }
        });
    });

    $("#download_bmp").click(function(){
        payload = { image_url: $("#svg_for_bmp").val(), file_type: "bmp" };

        $.ajax({
            url: "svg_to_raster",
            type: "GET",
            data: payload,
            success: function(data){
                $("#svg_to_bmp").append('<a href="/' + data.url + '" target="_BLANK">Download Results</a>');
            },
            error: function(data){

            }
        });
    });

    $("#ocr_scan").click(function(){
        $("#scanned_text").css("display", "block");

        $.ajax({
            url: "logo/ocr",
            type: "POST",
            async: true,
            success: function(data){
                if ( data.success == true ) {
                    $("#scanned_text").html( data.scanned );
                } else {
                    $("#scanned_text").html( "Error." );
                }
            },
            error: function(data){

            }
        });
    });


    // Integrate Boostrap 3 popovers.
    $(function () {
        $('[data-toggle="popover"]').popover({ html: true });
    })
});
