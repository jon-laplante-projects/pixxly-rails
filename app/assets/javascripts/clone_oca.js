$(function(){
    // params[:image_url]
    $(document).on ("click", "#clone_oca_link", function(){
        $("#pleaseWaitDialog").modal("show");

        $.getJSON("https://openclipart.org/search/json/?byids=" + Pixxly.Logos.current.oca_id, function(data){
            console.log(data["payload"][0]["svg"]["png_full_lossy"]);
            var payload = { url: data["payload"][0]["svg"]["png_full_lossy"], path: "" };

            if (data["payload"][0]["svg"]["png_full_lossy"] != null && data["payload"][0]["svg"]["png_full_lossy"] != "") {
                $.ajax({
                    url: "/import/store_raster",
                    type: "POST",
                    data: payload,
                    success: function(data){
                        if (data["success"] != false) {
                            window.location.href = "/logo";
                        } else {
                            $("#pleaseWaitDialog").modal("hide");

                            var notice = '<div class="alert alert-danger notify-top-right" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close" style="margin: 0 0 0 10px;"><span aria-hidden="true">&times;</span></button><i class="fa fa-exclamation-triangle"></i>&nbsp;There was a problem importing this symbol from OpenClipArt.<p/>Consider choosing a different symbol.</div>';

                            $("body").append(notice);
                        }

                    },
                    error: function(data){
                        $("#pleaseWaitDialog").modal("hide");

                        var notice = '<div class="alert alert-danger notify-top-right" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close" style="margin: 0 0 0 10px;"><span aria-hidden="true">&times;</span></button><i class="fa fa-exclamation-triangle"></i>&nbsp;There was a problem importing this symbol from OpenClipArt.<p/>Check your internet connection.</div>';

                        $("body").append(notice);
                    }
                });
            } else {
                $("#pleaseWaitDialog").modal("hide");

                var notice = '<div class="alert alert-danger notify-top-right" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close" style="margin: 0 0 0 10px;"><span aria-hidden="true">&times;</span></button><i class="fa fa-exclamation-triangle"></i>&nbsp;There was a problem importing this symbol from OpenClipArt.<p/>Check your internet connection.</div>';

                $("body").append(notice);
            }
        });
    });
});