$ ->
  buttonToggle = '.convert-or-embed'
  inputUrl = '#page_url'
  choiceActive = 'convert-choice-active'
  convertOrNo = '#convert_to_svg_checkbox'
  pathSelector = '#page_path'


  $(document).on 'click', buttonToggle, ->
    $(convertOrNo).val $(@).attr('data-convert')
    $(buttonToggle).removeClass choiceActive
    $(@).addClass choiceActive
    if $(@).attr('data-convert') == 'true'
      $('#black_and_white_toogle').show()
    else
      $('#black_and_white_toogle').hide()

  $(document).on "click", '.btn-clone-logo', ->
    if $(convertOrNo).val() == 'true'
      convertToSvg()
    else
      embedImage()

  convertToSvg = (payload) ->
    isColor = $("#image_is_color").is(':checked')
    if $(inputUrl).val() == ""
      payload = getPayload()
      payload.append 'is_color', isColor
      processData = false
      contentType = false
    else
      payload =
        image_url: $(inputUrl).val()
        is_color: isColor
      processData = true
      contentType = 'application/x-www-form-urlencoded'

    $.ajax(
      url: "/import/logo",
      type: "POST",
      data: payload,
      processData: processData,
      contentType: contentType,
      success: (data) ->
        if data.success == true
          if isColor
            location.reload()
          else
            generateIframe data["id"], data["svg"]

        else
          $("#cloneLogoSourceModal").modal("hide")
          alert("There was a problem converting your logo.")

        $(inputUrl).val("")
      error: (data) ->
        alert("Pixxly is having trouble parsing your image.")
        $(inputUrl).val("");
    )

  $(document).on "keyup", "#page_url", ->
    $("#page_path").val("")

  $(document).on "change", "#page_path", ->
    $("page_url").val("")

  getPayload = ->
    payload = new FormData
    payload.append 'img', $('#button_for_open_file').prop('files')[0]
    payload

  payloadOptions = ->
    if $("#page_path").val() != ""
      payload = new FormData
      payload.append 'path', $('#button_for_open_file').prop('files')[0]
      payload.append 'url', null
    else
      payload = { url: $("#page_url").val(), path: null }
    payload

  embedImage = ->
#if $(inputUrl).val() == ""
    payload = payloadOptions()
    #payload = getPayload()
    if $("#page_path").val() != ""
      processData = false
      contentType = false

      $.ajax(
        url: "/import/store_raster",
        type: "POST",
        data: payload,
        processData: processData,
        contentType: contentType,

        success: (data) ->
          if data.success == true
            window.location.href = "/logo"

        error: (data) ->
          alert("We couldn't embed this image. Is it a valid .jpg, .png, or .gif?")
          $(inputUrl).val("");
      )
    else
      $.ajax(
        url: "/import/store_raster",
        type: "POST",
        data: payload,

        success: (data) ->
          if data.success == true
            window.location.href = "/logo"

        error: (data) ->
          alert("We couldn't embed this image. Is it a valid .jpg, .png, or .gif?")
          $(inputUrl).val("");
      )
  #redirectToEditor $(inputUrl).val()

  redirectToEditor = (imageLink) ->
    location.assign '/embed_raster_to_svg?image=' + imageLink

  fieldsEmpty = ->
    urlEmpty = $(inputUrl).val().length == 0
    fileEmpty = $(pathSelector).val().length == 0

    if urlEmpty || fileEmpty
      true
    else
      false
    # fileEmpty && urlEmpty

  updateSubmitButtonState = ->
    button = $('.btn-clone-logo')
#    if fieldsEmpty()
#      button.prop('disabled', true)
#    else
#      button.prop('disabled', false)

  $(document).on 'show.bs.modal', '#cloneLogoSourceModal', updateSubmitButtonState

  $(document).on 'change', inputUrl, ->
    updateSubmitButtonState()
    $(pathSelector).val('')
  $(document).on 'change', pathSelector, ->
    updateSubmitButtonState()
    $(inputUrl).val('')