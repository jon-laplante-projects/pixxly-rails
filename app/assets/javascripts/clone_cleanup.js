// svgEdit = svgEdit || window.frames["logo_editor"];

function generateIframe(logo_id, svg){
    // $("body").append('<iframe src="/logo/editor" allowfullscreen frameborder="0" style="width: 100%; height: 700px; display: none;" id="logo_editor" class="logo_editor" data-user-logo-id="' + logo_id + '"></iframe>');

    // var tryFrame = setInterval(function(){
        if ( window.frames.length > 0 ) {
                // We've successfully loaded the iframe.
                var svgEdit = svgEdit || window.frames[0];
// debugger;
                // Push the cloned logo into the frame.
                svgEdit.svgCanvas.setSvgString($(svg).prop("outerHTML"));

                // Let's deal with the grouping.
                svgEdit.svgCanvas.selectAllInCurrentLayer();
                svgEdit.svgCanvas.ungroupSelectedElement();
                svgEdit.svgCanvas.selectAllInCurrentLayer();
                svgEdit.svgCanvas.groupSelectedElements();

                // Save the SVG back to the server.
                var payload = { user_logo_id: logo_id, svg: svgEdit.svgCanvas.getSvgString() };

                $.ajax({
                    url: "/logo/update",
                    type: "POST",
                    data: payload,
                    async: true,
                    success: function(data){
                        // $("#logo_editor").remove();

                        if (data.success === true) {
                            window.location = "/logo";
                        } else {
                            // $("#logo_editor").remove();

                            var notice = '<div class="alert alert-danger notify-top-right" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close" style="margin: 0 0 0 10px;"><span aria-hidden="true">&times;</span></button><i class="fa fa-exclamation-triangle"></i>&nbsp;There was a problem cloning this logo.<br/>Try finding it in another format(i.e. .jpg, .png, etc.), or converting it yourself.</div>';

                            parent.$("body").append(notice);

                        }
                    },
                    error: function(data){
                        // $("#logo_editor").remove();

                        var notice = '<div class="alert alert-danger notify-top-right" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close" style="margin: 0 0 0 10px;"><span aria-hidden="true">&times;</span></button><i class="fa fa-exclamation-triangle"></i>&nbsp;There was a problem cloning this logo.</div>';

                        parent.$("body").append(notice);
                    }
                });

                // Stop checking to see if the iframe is populated.
                clearInterval(tryFrame);
        }
    // }, 500);
}