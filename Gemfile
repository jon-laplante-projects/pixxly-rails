source 'https://rubygems.org'


# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.2.4'
gem 'pg' # Postgres Database
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.1.0'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'
# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'
# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0', group: :doc

gem "devise" # User authentication.
gem "cancancan" # User authorization.
gem "font-awesome-rails" # Font Awesome in the Rails pipeline.
gem "paperclip", '>= 4.3.0' # File upload/attachement.
gem "mini_magick" # Image editing wrapper using ImageMagick.
gem "rtesseract" # OCR package.
gem 'activeadmin', github: 'activeadmin' # Administrative interface for application.
gem 'aws-ses' # Provides an easy ruby DSL & interface to AWS SES
gem 'sprockets-rails'
gem 'bootstrap-sass', '~> 3.3.5' # Rails 4 injector for Bootstrap 3.
gem 'safe_attributes'
gem 'paypal-sdk-rest' # PayPal integration.
gem 'stripe' # Stripe integration.
gem 'kaminari'
gem 'bootstrap-kaminari-views'
gem 'browser' # Detect the browser, so we can hide layouts for I.E.
gem 'oga' # XML parser - testing at this point.
gem "active_admin_import" , '3.0.0.pre' # Gem to make importing CSV files into Active Admin easy.
gem 'active_admin_editor', github: 'boontdustie/active_admin_editor' # HTML WYSIWYG editor for Active Admin.
gem 'svg_optimizer'

gem 'rails-assets-twbs-pagination', source: 'https://rails-assets.org'
# Use Unicorn as the app server
# gem 'unicorn'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug'

  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'pry'
  gem 'guard'
  gem 'guard-shell'
  gem 'guard-livereload'
  gem 'capybara'
  gem 'rspec-rails'
  gem 'cucumber-rails', :require => false
  # database_cleaner is not required, but highly recommended
  gem 'database_cleaner'
  gem 'factory_girl_rails'
  gem 'seed_dump' # generates seed data.
  gem 'ffaker' # Faker rewrite for Rails testing.
  gem 'jasmine-rails' # Javascript unit testing.
  gem 'quiet_assets'
end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'web-console', '~> 2.0'
  gem 'thin'
end

group :test do
  gem 'poltergeist' # Headless JS testing, like Selenium without the browser.
end
