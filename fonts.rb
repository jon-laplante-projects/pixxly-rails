require 'rubygems'
require 'active_record'

def all_fonts
	Dir["#{Dir.pwd}/public/fonts/*.ttf"]
end

def font_file
	f = File.open("fonts.txt", "w")
		all_fonts.each do |font|
			f.write "<type\n"
			f.write "	format=\"ttf\"\n"
			f.write "	name=\"\"\n"
			f.write "	fullname=\"\"\n"
			f.write "	family=\"\"\n"
			f.write "	glyphs=\"/webapps/pixxly-rails/public/fonts/#{File.basename font}\"\n"
			f.write "	style=\"normal\"\n"
			f.write "	stretch=\"normal\"\n"
			f.write "	weight=\"400\"\n"
			f.write "/>\n\n"
		end
	f.close
end

font_file

puts "All fonts mapped."