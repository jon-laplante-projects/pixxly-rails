class CreateFonts < ActiveRecord::Migration
  def change
    create_table :fonts do |t|
      t.string :name
      t.string :url
      t.string :import
      t.text :font_face

      t.timestamps null: false
    end
  end
end
