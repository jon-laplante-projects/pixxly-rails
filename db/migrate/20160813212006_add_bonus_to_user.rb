class AddBonusToUser < ActiveRecord::Migration
  def change
    add_column :users, :bonus, :boolean
  end
end
