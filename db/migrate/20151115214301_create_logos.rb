class CreateLogos < ActiveRecord::Migration
  def change
    create_table :logos do |t|
      t.text :svg

      t.timestamps null: false
    end
  end
end
