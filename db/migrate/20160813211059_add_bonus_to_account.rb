class AddBonusToAccount < ActiveRecord::Migration
  def change
    add_column :accounts, :bonus, :boolean
  end
end
