class CreateSharedLogos < ActiveRecord::Migration
  def change
    create_table :shared_logos do |t|
      t.integer :user_logo_id
      t.text :svg

      t.timestamps null: false
    end
  end
end
