class AddEmbedCodeToHelpVideo < ActiveRecord::Migration
  def change
    add_column :help_videos, :embed_code, :string
  end
end
