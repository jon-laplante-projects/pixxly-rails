class CreateHelpVideos < ActiveRecord::Migration
  def change
    create_table :help_videos do |t|
      t.string :label
      t.string :descr
      t.string :embed_url

      t.timestamps null: false
    end
  end
end
