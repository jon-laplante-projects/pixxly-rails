class RemoveFirstNameAndLastNameFromAccount < ActiveRecord::Migration
  def change
    remove_column :accounts, :first_name
    remove_column :accounts, :surname
  end
end
