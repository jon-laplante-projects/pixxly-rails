class RenameBonusToBonusLogos < ActiveRecord::Migration
  def change
    rename_table :bonus, :bonus_logos
  end
end
