class CreateLayoutTemplates < ActiveRecord::Migration
  def change
    create_table :layout_templates do |t|
      t.string :label
      t.text :svg

      t.timestamps null: false
    end
  end
end
