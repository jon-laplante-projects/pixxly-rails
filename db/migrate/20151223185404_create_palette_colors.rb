class CreatePaletteColors < ActiveRecord::Migration
  def change
    create_table :palette_colors do |t|
      t.integer :palette_id
      t.string :color

      t.timestamps null: false
    end
  end
end
