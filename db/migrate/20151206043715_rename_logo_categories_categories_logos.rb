class RenameLogoCategoriesCategoriesLogos < ActiveRecord::Migration
  def change
    rename_table :logo_categories, :categories_logos
  end
end
