class AddImportToBonusFont < ActiveRecord::Migration
  def change
    add_column :bonus_fonts, :import, :string
  end
end
