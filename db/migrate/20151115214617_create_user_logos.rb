class CreateUserLogos < ActiveRecord::Migration
  def change
    create_table :user_logos do |t|
      t.integer :layout_template_id
      t.integer :logo_id
      t.integer :user_id
      t.text :svg

      t.timestamps null: false
    end
  end
end
