class RemoveBonusFromAccount < ActiveRecord::Migration
  def change
    remove_column :accounts, :bonus, :boolean
  end
end
