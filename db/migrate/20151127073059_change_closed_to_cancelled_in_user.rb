class ChangeClosedToCancelledInUser < ActiveRecord::Migration
  def change
    rename_column :users, :closed, :cancelled
  end
end
