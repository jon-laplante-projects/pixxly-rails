class CreateLeads < ActiveRecord::Migration
  def change
    create_table :leads do |t|
      t.string :first_name
      t.string :surname
      t.string :email
      t.string :phone
      t.boolean :opt_out

      t.timestamps null: false
    end
  end
end
