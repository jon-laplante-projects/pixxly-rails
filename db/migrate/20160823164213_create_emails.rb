class CreateEmails < ActiveRecord::Migration
  def change
    create_table :emails do |t|
      t.integer :admin_id
      t.string :name
      t.string :subj
      t.text :content
      t.string :list_type

      t.timestamps null: false
    end
  end
end
