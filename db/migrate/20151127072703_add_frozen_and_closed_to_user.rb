class AddFrozenAndClosedToUser < ActiveRecord::Migration
  def change
    add_column :users, :frozen, :boolean
    add_column :users, :closed, :boolean
  end
end
