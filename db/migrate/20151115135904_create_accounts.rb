class CreateAccounts < ActiveRecord::Migration
  def change
    create_table :accounts do |t|
      t.integer :user_id
      t.string :first_name
      t.string :surname
      t.string :status
      t.string :payment_processor
      t.integer :subscription_id
      t.boolean :demo_user
      t.boolean :admin_user

      t.timestamps null: false
    end
  end
end
