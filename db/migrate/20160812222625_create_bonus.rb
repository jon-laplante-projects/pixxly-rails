class CreateBonus < ActiveRecord::Migration
  def change
    create_table :bonus do |t|
      t.integer :user_logo_id
      t.text :svg
      t.integer :user_id

      t.timestamps null: false
    end
  end
end
