class CreateSharedLogoCategories < ActiveRecord::Migration
  def change
    create_table :shared_logo_categories do |t|
      t.integer :shared_logo_id
      t.integer :category_id

      t.timestamps null: false
    end
  end
end
