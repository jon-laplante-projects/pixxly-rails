class CreateBonusFonts < ActiveRecord::Migration
  def change
    create_table :bonus_fonts do |t|
      t.string :name
      t.string :url
      t.text :font_face
      t.string :font_family

      t.timestamps null: false
    end
  end
end
