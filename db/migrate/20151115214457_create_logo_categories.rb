class CreateLogoCategories < ActiveRecord::Migration
  def change
    create_table :logo_categories do |t|
      t.integer :logo_id
      t.integer :category_id

      t.timestamps null: false
    end
  end
end
