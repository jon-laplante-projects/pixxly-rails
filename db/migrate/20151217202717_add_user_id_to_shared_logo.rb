class AddUserIdToSharedLogo < ActiveRecord::Migration
  def change
    add_column :shared_logos, :user_id, :integer
  end
end
