class AddFontFamilyToFont < ActiveRecord::Migration
  def change
    add_column :fonts, :font_family, :string
  end
end
