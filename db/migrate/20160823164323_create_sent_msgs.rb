class CreateSentMsgs < ActiveRecord::Migration
  def change
    create_table :sent_msgs do |t|
      t.integer :lead_id
      t.integer :mail_id
      t.string :status

      t.timestamps null: false
    end
  end
end
