require 'rubygems'
require 'active_record'

ActiveRecord::Base.establish_connection(
	:adapter => "postgresql",
	:timeout => 5000,
	:encoding => "unicode",
	:database => "pixxly_production",
	:username => "root",
	:password => "n1md@_4lxx1p",
	:host => "localhost"
}

# ActiveRecord::Base.establish_connection(
# 	:adapter => "postgresql",
# 	:timeout => 5000,
# 	:encoding => "unicode",
# 	:database => "pixxly_development",
# 	:username => "pixxly_dev",
# 	:password => "p1xxl4",
# 	:host => "localhost"
# )

class Logo < ActiveRecord::Base
  has_and_belongs_to_many :categories
end

logos_json = File.read("logos.json")

logos = JSON.parse(logos_json)

logos.each do |l|
	logo = Logo.find(l["id"])
	logo.svg = l["svg"]
	logo.save
end

puts 'Logo SVG has been updated!'