require 'rubygems'
require 'active_record'

# ActiveRecord::Base.establish_connection(
# 	:adapter => "postgresql",
# 	#:pool => 5,
# 	:timeout => 5000,
# 	:encoding => "unicode",
# 	:database => "pixxly_development",
# 	:username => "pixxly_dev",
# 	:password => "p1xxl4",
# 	:host => "localhost"
# )

ActiveRecord::Base.establish_connection(
	:adapter => "postgresql",
	:timeout => 5000,
	:encoding => "unicode",
	:database => "pixxly_production",
	:username => "root",
	:password => "n1md@_4lxx1p",
	:host => "localhost"
)

class Palette < ActiveRecord::Base
  has_many :palette_colors
end

class PaletteColor < ActiveRecord::Base
  belongs_to :palette
end

class Layout < ActiveRecord::Base
end

palette_json = File.read("palettes.json")

palettes = JSON.parse(palette_json)

palettes.each do |p|
	palette = Palette.new
	palette.label = p["label"]
	palette.save
end

puts 'Palettes have been updated!'

color_json = File.read("colors.json")

colors = JSON.parse(color_json)

colors.each do |c|
	color = PaletteColor.new
	color.palette_id = c["palette_id"]
	color.color = c["color"]
	color.save
end

puts 'Palette colors have been updated!'

layout_json = File.read("layouts.json")

layouts = JSON.parse(layout_json)

layouts.each do |l|
	layout = Layout.new
	layout.label = l["label"]
	layout.svg = l["svg"]
	layout.image = l["image"]
	layout.save
end

puts 'Layouts have been updated!'