require 'rubygems'
require 'active_record'

# ActiveRecord::Base.establish_connection(
# 	:adapter => "postgresql",
# 	#:pool => 5,
# 	:timeout => 5000,
# 	:encoding => "unicode",
# 	:database => "pixxly_development",
# 	:username => "pixxly_dev",
# 	:password => "p1xxl4",
# 	:host => "localhost"
# )

ActiveRecord::Base.establish_connection(
	:adapter => "postgresql",
	:timeout => 5000,
	:encoding => "unicode",
	:database => "pixxly_production",
	:username => "root",
	:password => "n1md@_4lxx1p",
	:host => "localhost"
)

class Palette < ActiveRecord::Base
  has_many :palette_colors
end

class PaletteColor < ActiveRecord::Base
  belongs_to :palette
end

palette_json = File.read("palettes.json")

palettes = JSON.parse(palette_json)

palettes.each do |p|
	palette = Palette.new
	palette.label = p["name"]
	# palette.save

	p["colors"].each do |color|
		c = palette.palette_colors.new
		c.color = color
		c.save
	end
end

puts "Updates complete!"