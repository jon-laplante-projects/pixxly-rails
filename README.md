## Technologies used
* Ruby on Rails
* jQuery
* Method-Draw (JavaScript SVG editor)
* Bootstrap

## Ruby gems leveraged
* Devise (Auth)

![Pixxly Logo Creation App](https://axafjq.am.files.1drv.com/y4mS7gT-8Ouo7OLT4BsOu9TDjbflRxeXmdEkNHHbF_ms8ZPwXVc2Bp27Jy5NYGuzRvIzeK7p8n-olY71UiWmgOYZPI63BSC99-TmHjFrA3XXH2WGG5oQsX1I0LdEvs1x8bxG_2V73Gkzyo9HWlL2-noZfhQJOrh5liTnEV060ibiM0nJfNwReVYawHl6eOG4u9VmGZc9ezOszGNYCPvOUxKDg?width=1024&height=668&cropmode=none)

One of my favorite projects I've done in the past few years is a SaaS logo/emblem creation tool. This product was fun for me because it was a departure from the type of web projects I’d previously been involved in, and posed some interesting challenges.

## Features list
### My Logos
* Edit Logo
* Share Logo
* Delete Logo
* Export as PNG
* Export as JPG
* Export as GIF
* Export as SVG
* Export as PSD
* Export as TIFF
* Export as WMF
* Preview
* Layout

### Library
* Clone in My Logos
* Add/Remove

### New Logo
* Choose from OCA
* Use built-in shape
* All layouts
* Edit and save
* Change Category
* Color Palettes
* Go Back to Previous
* Font Manager
* Layer Manager

![Logo Creator - Open ClipArt Import](https://qbuggq.am.files.1drv.com/y4mvzJ18bIV0XSKkaZpRd8ZqvWM8Ufaw9lUv_icv6XP75mUf6wG1FVPIUOwT88NFKZOiWeg7cSEONAuE8Hj2w23FbZQVkZezZ0jFr_Ypzn_-jZFrBxWAxci5Fm-jO6dky7NWhu-QhX0X8SXg05lMbsVF-Rw6d5msCPlmvc9SY5R9GBtVlYSz_yilSxVRZo-JUxsG8BUyrY23-7qeTSvVzR-Gg?width=1024&height=658&cropmode=none)
![Logo Creator - Color Palette Chooser](https://bbydgq.am.files.1drv.com/y4mSiU8VYCVyICpphrJ8ZDAJklK1nGUHybBw8i2nN3GJy8SaCm-2p1iKDjyWZ54Mwku3m3zkOJZAcJW4R5kZHFOFcb-zQnw6nBKC6j1jfKw8bQfQgYnfkuL8W4tjHIFbvP_OpNJuJ98JwAJdEmJlg33FBPTEAzkcHMX1-7xbVHqyfJB4o2Wy2FlUmr4ySb6ctO9zyqRpXkNuNXgu9G1qKG9eA?width=660&height=425&cropmode=none)

### Clone Logo
* Clone to SVG Outline
* Embed

### Profiles
* Change name
* Change password
* Quit

### Editor (Method Draw)
![Logo Creator - Choose a logo shape](https://pxudgq.am.files.1drv.com/y4m9kTvraEBUuFrRA9Q1ecJ5xi-P2_jN9Bf4hi55rbfJEgvZB3mqION6EDSXAgalq48LFWJ8dlfRqZGHwWYknHcLdbGaF41b7Y6EKrPL4P66F3ZuemAJrGgHZs1T6krNnWCONeTqVbDxg7MLAzJg7b3qPWfSJs_Q9BfCyT-PD7wYPtWwwtM0qyjTlqUT1CxV1b_Pa2oWuTjZkzCk-kcafahrg?width=660&height=414&cropmode=none)

#### Select Tool	
* Canvas Width
* Canvas Height
* Color
* Sizes

#### Pencil Tool	
* Drawing
* X
* Y
* Rotation
* Opacity
* Blur
* Align Left
* Align Right
* Align Center
* Align Top
* Align Middle
* Align Bottom
* Stroke Width
* Stroke Style

#### Line Tool	
* Start X
* End X
* Start Y
* End Y
* Rotation
* Opacity
* Blur
* Align Left
* Align Right
* Align Center
* Align Top
* Align Middle
* Align Bottom
* Stroke Width
* Stroke Style

#### Square/Rect Tool	
* X
* Y
* Width
* Height
* Roundness
* Rotation
* Opacity
* Blur
* Align Left
* Align Right
* Align Center
* Align Top
* Align Middle
* Align Bottom
* Stroke Width
* Stroke Style

#### Ellipse/Circle Tool	
* X
* Y
* Radius X
* Radius Y
* Rotation
* Opacity
* Blur
* Align Left
* Align Right
* Align Center
* Align Top
* Align Middle
* Align Bottom
* Stroke Width
* Stroke Style

#### Path Tool
* X
* Y
* Rotation
* Opacity
* Blur
* Align Left
* Align Right
* Align Center
* Align Top
* Align Middle
* Align Bottom
* Stroke Width
* Stroke Style
* Segment Type
* Add Node
* Delete Node
* Open Path

#### Shape Library	
* X
* Y
* Rotation
* Opacity
* Blur
* Align Left
* Align Right
* Align Center
* Align Top
* Align Middle
* Align Bottom
* Stroke Width
* Stroke Style

#### Text Tool	
* X
* Y
* Font
* Bold
* Italics
* Font Size
* Rotation
* Opacity
* Blur
* Align Left
* Align Right
* Align Center
* Align Top
* Align Middle
* Align Bottom
* Stroke Width
* Stroke Style

#### Zoom Tool	
* Zoom in

#### Eye Dropper Tool	
* Select color

#### Flood fill	
* Color change

#### Color Palette	
* Change fore color
* Change bg color

#### Status bar zoom	
* Change zoom level

#### Status bar save	
* Save

#### Status bar fullscreen	
* Fullscreen
* Windowed

#### File	
* New Document
* Import Image…

#### Edit	
* Undo
* Redo
* Cut
* Copy
* Paste
* Duplicate
* Delete

#### Object	
* Bring to Front
* Bring Forward
* Send Backward
* Send to Back
* Group Elements
* Ungroup Elements

#### Context Menu	
* Cut
* Copy
* Paste
* Delete
* Bring to Front
* Bring Forward
* Send Backward
* Send to Back