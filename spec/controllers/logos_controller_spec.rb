require 'rails_helper'

describe LogoController do
  describe "GET #index" do
    context 'with valid attributes' do
      it 'returns a JSON list of user-owned logos' do

      end
    end

    context 'with invalid attributes' do
      it 'returns JSON warning of failure' do

      end
    end
  end

  describe "GET #library" do
    context 'with valid attributes' do
      it 'returns a JSON list of user-owned logos' do

      end
    end

    context 'with invalid attributes' do
      it 'returns JSON warning of failure' do

      end
    end
  end

  describe "GET #library_editor" do
    context 'with valid attributes' do
      it 'returns a JSON list of user-owned logos' do

      end
    end

    context 'with invalid attributes' do
      it 'returns JSON warning of failure' do

      end
    end
  end

  describe "GET #new" do
    context 'with valid attributes' do
      it 'returns a JSON list of user-owned logos' do

      end
    end

    context 'with invalid attributes' do
      it 'returns JSON warning of failure' do

      end
    end
  end

  describe "GET #clone" do
    context 'with valid attributes' do
      it 'returns a JSON list of user-owned logos' do

      end
    end

    context 'with invalid attributes' do
      it 'returns JSON warning of failure' do

      end
    end
  end
end

# context 'with valid attributes' do
#   it 'creates the vehicle' do
#     post :create, vehicle: attributes_for(:vehicle)
#     expect(Vehicle.count).to eq(1)
#   end
#
#   it 'redirects to the "show" action for the new vehicle' do
#     post :create, vehicle: attributes_for(:vehicle)
#     expect(response).to redirect_to Vehicle.first
#   end
# end
# context 'with invalid attributes' do
#   it 'does not create the vehicle' do
#     post :create, vehicle: attributes_for(:vehicle, year: nil)
#     expect(Vehicle.count).to eq(0)
#   end
#
#   it 're-renders the "new" view' do
#     post :create, vehicle: attributes_for(:vehicle, year: nil)
#     expect(response).to render_template :new
#   end
# end