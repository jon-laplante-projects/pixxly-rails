FactoryGirl.define do
  factory :account do
    user_id 1
    status "active"
    payment_processor "paypal"
    subscription_id 10
    demo_user false
    admin_user false
  end
end
