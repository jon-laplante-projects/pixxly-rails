FactoryGirl.define do
  factory :category do
    label { FFaker::Lorem.word }
  end

end
