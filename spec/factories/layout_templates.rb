FactoryGirl.define do
  factory :layout_template do
    label { FFaker::Lorem.word }
    # TODO: Add actual SVG to test.
    svg "Placeholder."
  end

end
