FactoryGirl.define do
  factory :user_logo do
    layout_template_id Random.rand(20)
    logo_id Random.rand(20)
    user_id Random.rand(20)
    # TODO: Add actual SVG to test.
    svg "Placeholder."
  end

end
