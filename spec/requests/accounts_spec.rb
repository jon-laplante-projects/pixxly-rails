require 'spec_helper'
require 'ffaker'

describe "Users", js: true do
 it "validates login" do
  visit new_user_session_path
   fill_in "user_email", "jonlaplante@hotmail.com"
   fill_in "user_password", "12345678"

   click_button 'Sign in >>'

   expect(page).to have_content "My Logos"
 end

 it "validates sign up form" do
   pwd = "87654321"
   visit new_user_registration_path
   fill_in "First Name", "Testy"
   fill_in "Last Name", "McTesterson"
   fill_in "Email", "test@test.tst"
   fill_in "Password", "#{pwd}"
   fill_in "Confirm Password", "#{pwd}"
   fill_in "Credit Card Number", "4111 1111 1111 1111"
   fill_in "Expiry Month", "May"
   fill_in "Expiry Year", "2018"
   fill_in "CVV (on the back of your card)", "411"

   click_button "Buy Now!"

   expect(page).to have_content "My Logos"
 end
end

describe "Accounts" do
  # Placeholder.
end