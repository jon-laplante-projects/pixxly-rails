require 'rails_helper'

RSpec.describe Category, type: :model do
  it 'has a valid factory' do
    expect(build(:category)).to be_valid
  end

  it 'is invalid without a label' do
    expect(build(:category, label: nil)).to_not be_valid
  end
end
