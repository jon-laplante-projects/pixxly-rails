require 'rails_helper'

RSpec.describe CategoriesLogos, type: :model do
  it 'has a valid factory' do
    expect(build(:logo_category)).to be_valid
  end

  it 'is invalid without a logo_id' do
    expect(build(:logo_category, logo_id: nil)).to_not be_valid
  end

  it 'is invalid without a category_id' do
    expect(build(:logo_category, category_id: nil)).to_not be_valid
  end
end
