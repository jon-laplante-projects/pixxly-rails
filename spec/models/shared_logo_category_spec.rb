require 'rails_helper'

RSpec.describe SharedLogoCategory, type: :model do
  it 'has a valid factory' do
    expect(build(:shared_logo_category)).to be_valid
  end

  it 'is invalid without a shared_logo_id' do
    expect(build(:shared_logo_category, shared_logo_id: nil)).to_not be_valid
  end

  it 'is invalid without a category_id' do
    expect(build(:shared_logo_category, category_id: nil)).to_not be_valid
  end
end
