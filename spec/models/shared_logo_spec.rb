require 'rails_helper'

RSpec.describe SharedLogo, type: :model do
  # TODO: Add actual SVG to test.
  it 'has a valid factory' do
    expect(build(:shared_logo)).to be_valid
  end

  it 'is invalid without a svg' do
    expect(build(:shared_logo, svg: nil)).to_not be_valid
  end
end
