require 'rails_helper'

RSpec.describe LayoutTemplate, type: :model do
  it 'has a valid factory' do
    expect(build(:layout_template)).to be_valid
  end

  it 'is invalid without a label' do
    expect(build(:layout_template, label: nil)).to_not be_valid
  end

  it 'is invalid without svg' do
    expect(build(:layout_template, svg: nil)).to_not be_valid
  end
end
