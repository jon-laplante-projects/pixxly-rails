require 'rails_helper'

RSpec.describe Account, type: :model do
  it 'has a valid factory' do
    expect(build(:account)).to be_valid
  end

  it 'is invalid without status' do
    expect(build(:account, status: nil)).to_not be_valid
  end

  it 'is invalid without a payment_processor' do
    expect(build(:account, payment_processor: nil)).to_not be_valid
  end

  it 'is invalid without a demo flag' do
    expect(build(:account, demo: nil)).to_not be_valid
  end

  it 'is invalid without an admin flag' do
    expect(build(:account, admin: nil)).to_not be_valid
  end
end
