require 'rails_helper'

RSpec.describe Logo, type: :model do
  # TODO: Add actual SVG to test.
  it 'has a valid factory' do
    expect(build(:logo)).to be_valid
  end

  it 'is invalid without svg' do
    expect(build(:logo, svg: nil)).to_not be_valid
  end
end
