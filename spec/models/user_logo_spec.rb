require 'rails_helper'

RSpec.describe UserLogo, type: :model do
  it 'has a valid factory' do
    expect(build(:user_logo)).to be_valid
  end

  it 'is invalid without layout_template_id' do
    expect(build(:user_logo, layout_template_id: nil)).to_not be_valid
  end

  it 'is invalid without logo_id' do
    expect(build(:user_logo, logo_id: nil)).to_not be_valid
  end

  it 'is invalid without user_id' do
    expect(build(:user_logo, user_id: nil)).to_not be_valid
  end
end
