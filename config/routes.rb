Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  mount JasmineRails::Engine => '/specs' if defined?(JasmineRails)
  ActiveAdmin.routes(self)
  devise_for :users, controllers: { registrations: "users/registrations", sessions: "users/sessions" }, :path => '', :path_names => {:sign_in => 'login', :sign_out => 'logout'}

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'logo#index'
  # root 'welcome#index'

  # authenticated :user do
  #   root to: 'logo#index', as: :authenticated_root
  # end

  get "logo" => 'logo#index'
  get "logo/library" => 'logo#library'
  get "logo/library/editor/:library_logo_id" => 'logo#library_editor'
  get "logo/bonus", to: 'logo#bonus'
  post "logo/shared/save", to: "logo#import_from_library"
  post "logo/bonus/save", to: "logo#import_from_bonus"
  get "logo/new" => 'logo#new'
  get "edit/:user_logo_id", to: 'logo#edit_logo'
  get 'embed_raster_to_svg', to: 'logo#embed_raster_to_svg'
  post '/import/store_raster', to: 'logo#store_raster'
  get "logo/editor" => 'logo#editor'
  get "logo/clone" => 'logo#clone'
  post "logo/duplicate", to: 'logo#clone_user_logo'
  post "logo/save", to: 'logo#save'
  post "logo/update", to: 'logo#update_user_logo'
  get "logo/delete", to: 'logo#delete_user_logo'
  post "logo/delete", to: 'logo#ajax_delete_user_logo'
  get "logo/:logo_id/as/file", to: 'logo#as_file'
  get "user/logo/:logo_id/as/file", to: 'logo#user_logo_as_file'
  get "shared/logo/:logo_id/as/file", to: 'logo#shared_logo_as_file'
  get "logo/categories/list", to: 'logo#categories_list'
  get "logo/:logo_id/json", to: 'logo#as_json'
  get "logo/:category_id/set", to: 'logo#symbols_set'
  post "logo/share", to: 'logo#share_logo'
  # post "import/logo", to: 'logo#raster_to_svg'
  post "import/logo", to: 'logo#clone'
  post "logo/svg", to: 'logo#get_svg'

  get "export/raster/:file_type/:logo_id/:size", to: 'logo#svg_to_raster'

  get "experimental", to: 'experimental#index'
  get "raster_to_svg", to: 'experimental#raster_to_svg'
  get "svg_to_raster", to: 'experimental#svg_to_raster'
  get "svg_to_psd", to: 'experimental#svg_to_psd'
  post "logo/ocr", to: 'experimental#logo_ocr'

  get "import/logo/page", to: 'logo#get_page'
  post "import/logo/page", to: 'logo#get_page'

  get "logo/openclipart_download", to: 'logo#openclipart_download'

  get "color/palettes", to: 'logo#get_palettes'
  get "layouts/list", to: 'logo#get_layouts'
  get "all/colors", to: 'logo#all_colors'
  get "all/palettes", to: 'logo#all_palettes'

  post 'accounts/zaxaa', to: "welcome#zaxaa_zpn"
  match 'accounts/zaxaa', to: 'welcome#zaxaa_zpn', :constraints => { :protocol => 'https' }, :via => :get

  post 'accounts/jvz', to: "welcome#jvzoo"
  match 'accounts/jvz', to: 'welcome#jvzoo', :constraints => { :protocol => 'https' }, :via => :get
  
  get 'help', to: "help_videos#index"
end
